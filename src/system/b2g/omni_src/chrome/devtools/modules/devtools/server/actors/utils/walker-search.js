"use strict";const{Ci,Cu}=require("chrome");function WalkerIndex(walker){this.walker=walker;this.clearIndex=this.clearIndex.bind(this);this.walker.on("any-mutation",this.clearIndex);}
WalkerIndex.prototype={destroy:function(){this.walker.off("any-mutation",this.clearIndex);},clearIndex:function(){if(!this.currentlyIndexing){this._data=null;}},get doc(){return this.walker.rootDoc;},get data(){if(!this._data){this._data=new Map();this.index();}
return this._data;},_addToIndex:function(type,node,value){ let entry=this._data.get(value);if(!entry){this._data.set(value,[]);} 
this._data.get(value).push({type:type,node:node});},index:function(){
this.currentlyIndexing=true;let documentWalker=this.walker.getDocumentWalker(this.doc);while(documentWalker.nextNode()){let node=documentWalker.currentNode;if(node.nodeType===1){
 let localName=node.localName;if(localName==="_moz_generated_content_before"){this._addToIndex("tag",node,"::before");this._addToIndex("text",node,node.textContent.trim());}else if(localName==="_moz_generated_content_after"){this._addToIndex("tag",node,"::after");this._addToIndex("text",node,node.textContent.trim());}else{this._addToIndex("tag",node,node.localName);}
for(let{name,value}of node.attributes){this._addToIndex("attributeName",node,name);this._addToIndex("attributeValue",node,value);}}else if(node.textContent&&node.textContent.trim().length){ this._addToIndex("text",node,node.textContent.trim());}}
this.currentlyIndexing=false;}};exports.WalkerIndex=WalkerIndex;function WalkerSearch(walker){this.walker=walker;this.index=new WalkerIndex(this.walker);}
WalkerSearch.prototype={destroy:function(){this.index.destroy();this.walker=null;},_addResult:function(node,type,results){if(!results.has(node)){results.set(node,[]);}
let matches=results.get(node); let isKnown=false;for(let match of matches){if(match.type===type){isKnown=true;break;}}
if(!isKnown){matches.push({type});}},_searchIndex:function(query,options,results){for(let[matched,res]of this.index.data){if(!options.searchMethod(query,matched)){continue;}
res.filter(entry=>{return options.types.indexOf(entry.type)!==-1;}).forEach(({node,type})=>{this._addResult(node,type,results);});}},_searchSelectors:function(query,options,results){
 let isSelector=query&&query.match(/[ >~.#\[\]]/);if(options.types.indexOf("selector")===-1||!isSelector){return;}
let nodes=this.walker._multiFrameQuerySelectorAll(query);for(let node of nodes){this._addResult(node,"selector",results);}},search:function(query,options={}){options.searchMethod=options.searchMethod||WalkerSearch.SEARCH_METHOD_CONTAINS;options.types=options.types||WalkerSearch.ALL_RESULTS_TYPES; if(typeof query!=="string"){query="";} 
let results=new Map(); this._searchIndex(query,options,results); this._searchSelectors(query,options,results); let resultList=[];for(let[node,matches]of results){for(let{type}of matches){resultList.push({node:node,type:type,});

break;}}
let documents=this.walker.tabActor.windows.map(win=>win.document); resultList.sort((a,b)=>{
if(a.node.ownerDocument!=b.node.ownerDocument){let indA=documents.indexOf(a.node.ownerDocument);let indB=documents.indexOf(b.node.ownerDocument);return indA-indB;}
return a.node.compareDocumentPosition(b.node)&4?-1:1;});return resultList;}};WalkerSearch.SEARCH_METHOD_CONTAINS=(query,candidate)=>{return query&&candidate.toLowerCase().indexOf(query.toLowerCase())!==-1;};WalkerSearch.ALL_RESULTS_TYPES=["tag","text","attributeName","attributeValue","selector"];exports.WalkerSearch=WalkerSearch;
(function(root,factory){"use strict";if(typeof define==="function"&&define.amd){define(factory);}else if(typeof exports==="object"){module.exports=factory();}else{root.prettyFast=factory();}}(this,function(){"use strict";var acorn=this.acorn||require("acorn/acorn");var sourceMap=this.sourceMap||require("source-map");var SourceNode=sourceMap.SourceNode;




var PRE_ARRAY_LITERAL_TOKENS={"typeof":true,"void":true,"delete":true,"case":true,"do":true,"=":true,"in":true,"{":true,"*":true,"/":true,"%":true,"else":true,";":true,"++":true,"--":true,"+":true,"-":true,"~":true,"!":true,":":true,"?":true,">>":true,">>>":true,"<<":true,"||":true,"&&":true,"<":true,">":true,"<=":true,">=":true,"instanceof":true,"&":true,"^":true,"|":true,"==":true,"!=":true,"===":true,"!==":true,",":true,"}":true};function isArrayLiteral(token,lastToken){if(token.type.label!="["){return false;}
if(!lastToken){return true;}
if(lastToken.type.isAssign){return true;}
return!!PRE_ARRAY_LITERAL_TOKENS[lastToken.type.keyword||lastToken.type.label];}

var PREVENT_ASI_AFTER_TOKENS={"*":true,"/":true,"%":true,"+":true,"-":true,"<<":true,">>":true,">>>":true,"<":true,">":true,"<=":true,">=":true,"instanceof":true,"in":true,"==":true,"!=":true,"===":true,"!==":true,"&":true,"^":true,"|":true,"&&":true,"||":true,",":true,".":true,"=":true,"*=":true,"/=":true,"%=":true,"+=":true,"-=":true,"<<=":true,">>=":true,">>>=":true,"&=":true,"^=":true,"|=":true,"delete":true,"void":true,"typeof":true,"~":true,"!":true,"new":true,"(":true};
var PREVENT_ASI_BEFORE_TOKENS={"*":true,"/":true,"%":true,"<<":true,">>":true,">>>":true,"<":true,">":true,"<=":true,">=":true,"instanceof":true,"in":true,"==":true,"!=":true,"===":true,"!==":true,"&":true,"^":true,"|":true,"&&":true,"||":true,",":true,".":true,"=":true,"*=":true,"/=":true,"%=":true,"+=":true,"-=":true,"<<=":true,">>=":true,">>>=":true,"&=":true,"^=":true,"|=":true,"(":true};function isASI(token,lastToken){if(!lastToken){return false;}
if(token.loc.start.line===lastToken.loc.start.line){return false;}
if(PREVENT_ASI_AFTER_TOKENS[lastToken.type.label||lastToken.type.keyword]){return false;}
if(PREVENT_ASI_BEFORE_TOKENS[token.type.label||token.type.keyword]){return false;}
return true;}
function isGetterOrSetter(token,lastToken,stack){return stack[stack.length-1]=="{"&&lastToken&&lastToken.type.label=="name"&&(lastToken.value=="get"||lastToken.value=="set")&&token.type.label=="name";}
function isLineDelimiter(token,stack){if(token.isArrayLiteral){return true;}
var ttl=token.type.label;var top=stack[stack.length-1];return ttl==";"&&top!="("||ttl=="{"||ttl==","&&top!="("||ttl==":"&&(top=="case"||top=="default");}
function appendNewline(token,write,stack){if(isLineDelimiter(token,stack)){write("\n",token.loc.start.line,token.loc.start.column);return true;}
return false;}
function needsSpaceAfter(token,lastToken){if(lastToken){if(lastToken.type.isLoop){return true;}
if(lastToken.type.isAssign){return true;}
if(lastToken.type.binop!=null){return true;}
var ltt=lastToken.type.label;if(ltt=="?"){return true;}
if(ltt==":"){return true;}
if(ltt==","){return true;}
if(ltt==";"){return true;}
var ltk=lastToken.type.keyword;if(ltk!=null){if(ltk=="break"||ltk=="continue"||ltk=="return"){return token.type.label!=";";}
if(ltk!="debugger"&&ltk!="null"&&ltk!="true"&&ltk!="false"&&ltk!="this"&&ltk!="default"){return true;}}
if(ltt==")"&&(token.type.label!=")"&&token.type.label!="]"&&token.type.label!=";"&&token.type.label!=","&&token.type.label!=".")){return true;}}
if(token.type.isAssign){return true;}
if(token.type.binop!=null){return true;}
if(token.type.label=="?"){return true;}
return false;}
function prependWhiteSpace(token,lastToken,addedNewline,write,options,indentLevel,stack){var ttk=token.type.keyword;var ttl=token.type.label;var newlineAdded=addedNewline;var ltt=lastToken?lastToken.type.label:null;


if(lastToken&&ltt=="}"){if(ttk=="while"&&stack[stack.length-1]=="do"){write(" ",lastToken.loc.start.line,lastToken.loc.start.column);}else if(ttk=="else"||ttk=="catch"||ttk=="finally"){write(" ",lastToken.loc.start.line,lastToken.loc.start.column);}else if(ttl!="("&&ttl!=";"&&ttl!=","&&ttl!=")"&&ttl!="."){write("\n",lastToken.loc.start.line,lastToken.loc.start.column);newlineAdded=true;}}
if(isGetterOrSetter(token,lastToken,stack)){write(" ",lastToken.loc.start.line,lastToken.loc.start.column);}
if(ttl==":"&&stack[stack.length-1]=="?"){write(" ",lastToken.loc.start.line,lastToken.loc.start.column);}
if(lastToken&&ltt!="}"&&ttk=="else"){write(" ",lastToken.loc.start.line,lastToken.loc.start.column);}
function ensureNewline(){if(!newlineAdded){write("\n",lastToken.loc.start.line,lastToken.loc.start.column);newlineAdded=true;}}
if(isASI(token,lastToken)){ensureNewline();}
if(decrementsIndent(ttl,stack)){ensureNewline();}
if(newlineAdded){if(ttk=="case"||ttk=="default"){write(repeat(options.indent,indentLevel-1),token.loc.start.line,token.loc.start.column);}else{write(repeat(options.indent,indentLevel),token.loc.start.line,token.loc.start.column);}}else if(needsSpaceAfter(token,lastToken)){write(" ",lastToken.loc.start.line,lastToken.loc.start.column);}}
function repeat(str,n){var result="";while(n>0){if(n&1){result+=str;}
n>>=1;str+=str;}
return result;}
var sanitize=(function(){var escapeCharacters={"\\":"\\\\","\n":"\\n","\r":"\\r","\t":"\\t","\v":"\\v","\f":"\\f","\0":"\\0","'":"\\'"};var regExpString="("
+Object.keys(escapeCharacters).map(function(c){return escapeCharacters[c];}).join("|")
+")";var escapeCharactersRegExp=new RegExp(regExpString,"g");return function(str){return str.replace(escapeCharactersRegExp,function(_,c){return escapeCharacters[c];});};}());function addToken(token,write){if(token.type.label=="string"){write("'"+sanitize(token.value)+"'",token.loc.start.line,token.loc.start.column);}else if(token.type.label=="regexp"){write(String(token.value.value),token.loc.start.line,token.loc.start.column);}else{write(String(token.value!=null?token.value:token.type.label),token.loc.start.line,token.loc.start.column);}}
function belongsOnStack(token){var ttl=token.type.label;var ttk=token.type.keyword;return ttl=="{"||ttl=="("||ttl=="["||ttl=="?"||ttk=="do"||ttk=="switch"||ttk=="case"||ttk=="default";}
function shouldStackPop(token,stack){var ttl=token.type.label;var ttk=token.type.keyword;var top=stack[stack.length-1];return ttl=="]"||ttl==")"||ttl=="}"||(ttl==":"&&(top=="case"||top=="default"||top=="?"))||(ttk=="while"&&top=="do");}
function decrementsIndent(tokenType,stack){return tokenType=="}"||(tokenType=="]"&&stack[stack.length-1]=="[\n");}
function incrementsIndent(token){return token.type.label=="{"||token.isArrayLiteral||token.type.keyword=="switch";}
function addComment(write,indentLevel,options,block,text,line,column){var indentString=repeat(options.indent,indentLevel);write(indentString,line,column);if(block){write("/*");write(text.split(new RegExp("/\n"+indentString+"/","g")).join("\n"+indentString));write("*/");}else{write("//");write(text);}
write("\n");}
return function prettyFast(input,options){var indentLevel=0;var result=new SourceNode();var write=(function(){var buffer=[];var bufferLine=-1;var bufferColumn=-1;return function write(str,line,column){if(line!=null&&bufferLine===-1){bufferLine=line;}
if(column!=null&&bufferColumn===-1){bufferColumn=column;}
buffer.push(str);if(str=="\n"){var lineStr="";for(var i=0,len=buffer.length;i<len;i++){lineStr+=buffer[i];}
result.add(new SourceNode(bufferLine,bufferColumn,options.url,lineStr));buffer.splice(0,buffer.length);bufferLine=-1;bufferColumn=-1;}};}());var addedNewline=false;var token;
var ttl;
var ttk;var lastToken;





var stack=[];




var tokenQueue=[];var tokens=acorn.tokenizer(input,{locations:true,sourceFile:options.url,onComment:function(block,text,start,end,startLoc,endLoc){tokenQueue.push({type:{},comment:true,block:block,text:text,loc:{start:startLoc,end:endLoc}});}});for(;;){token=tokens.getToken();tokenQueue.push(token);if(token.type.label=="eof"){break;}}
for(var i=0;i<tokenQueue.length;i++){token=tokenQueue[i];if(token.comment){var commentIndentLevel=indentLevel;if(lastToken&&(lastToken.loc.end.line==token.loc.start.line)){commentIndentLevel=0;write(" ");}
addComment(write,commentIndentLevel,options,token.block,token.text,token.loc.start.line,token.loc.start.column);addedNewline=true;continue;}
ttk=token.type.keyword;ttl=token.type.label;if(ttl=="eof"){if(!addedNewline){write("\n");}
break;}
token.isArrayLiteral=isArrayLiteral(token,lastToken);if(belongsOnStack(token)){if(token.isArrayLiteral){stack.push("[\n");}else{stack.push(ttl||ttk);}}
if(decrementsIndent(ttl,stack)){indentLevel--;if(ttl=="}"&&stack.length>1&&stack[stack.length-2]=="switch"){indentLevel--;}}
prependWhiteSpace(token,lastToken,addedNewline,write,options,indentLevel,stack);addToken(token,write); var nextToken=tokenQueue[i+1];if(!nextToken||!nextToken.comment||token.loc.end.line!=nextToken.loc.start.line){addedNewline=appendNewline(token,write,stack);}
if(shouldStackPop(token,stack)){stack.pop();if(token=="}"&&stack.length&&stack[stack.length-1]=="switch"){stack.pop();}}
if(incrementsIndent(token)){indentLevel++;}



if(!lastToken){lastToken={loc:{start:{},end:{}}};}
lastToken.start=token.start;lastToken.end=token.end;lastToken.loc.start.line=token.loc.start.line;lastToken.loc.start.column=token.loc.start.column;lastToken.loc.end.line=token.loc.end.line;lastToken.loc.end.column=token.loc.end.column;lastToken.type=token.type;lastToken.value=token.value;lastToken.isArrayLiteral=token.isArrayLiteral;}
return result.toStringWithSourceMap({file:options.url});};}.bind(this)));
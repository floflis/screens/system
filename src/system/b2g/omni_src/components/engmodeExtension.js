'use strict';
let DEBUG = 1;
let this_self;
var debug = function(s) {
    if (DEBUG) {
        dump('<KAIOS_LOG> -*- EngmodeExtension -*-: ' + s + '\n');
    }
};
const Cc = Components.classes;
const Ci = Components.interfaces;
const Cu = Components.utils;
Cu.import('resource://gre/modules/XPCOMUtils.jsm');
Cu.import('resource://gre/modules/Services.jsm');
Cu.import('resource://gre/modules/DOMRequestHelper.jsm');
Cu.import('resource://gre/modules/FileUtils.jsm');
Cu.import('resource://gre/modules/Timer.jsm');
Cu.import('resource://gre/modules/NetUtil.jsm');
XPCOMUtils.defineLazyGetter(this, 'libcutils', function() {
    Cu.import('resource://gre/modules/systemlibs.js');
    return libcutils;
});
XPCOMUtils.defineLazyServiceGetter(this, 'cpmm', '@mozilla.org/childprocessmessagemanager;1', 'nsIMessageSender');
const NETWORK_TYPE_UNKNOWN = -1;
const NETWORK_TYPE_WIFI = 0;
const NETWORK_TYPE_MOBILE = 1;
const NETWORK_TYPE_MOBILE_MMS = 2;
const NETWORK_TYPE_MOBILE_SUPL = 3;
const NETWORK_TYPE_WIFI_P2P = 4;
const NETWORK_TYPE_MOBILE_IMS = 5;
const NETWORK_TYPE_MOBILE_DUN = 6;
const NETWORK_TYPE_MOBILE_OMADM = 8;
const UNKNOWN_CLASS = -1;
const INET_CLASS = 0;
const IMS_CLASS = 2;
const ADMN_CLASS = 3;
const APP_CLASS = 4;
const TCTOMA_APN_TYPE_MAX = 4;

function engmodeIDOMExtension() {}

function exposeReadOnly(data) {
    return Cu.cloneInto(data, this_self._window);
}
engmodeIDOMExtension.prototype = {
    __proto__: DOMRequestIpcHelper.prototype,
    classDescription: 'The engmode extension for DOM',
    classID: Components.ID('{69ed1d4f-ac5a-44d9-80cb-d4e70a6f71df}'),
    contractID: '@kaiostech.com/extension;1',
    classInfo: XPCOMUtils.generateCI({
        classID: Components.ID('{69ed1d4f-ac5a-44d9-80cb-d4e70a6f71df}'),
        ontractID: '@kaiostech.com/extension;1',
        classDescription: 'engmodeExtension',
        interfaces: [Ci.engmodeIDOMExtension],
        flags: Ci.nsIClassInfo.DOM_OBJECT
    }),
    QueryInterface: XPCOMUtils.generateQI([Ci.engmodeIDOMExtension, Ci.nsIDOMGlobalPropertyInitializer, Ci.nsIMessageListener, Ci.nsISupportsWeakReference, Ci.nsIObserver]),
    _getOemfuseCb: null,
    init: function(aWindow) {
        debug('Initialized');
        this_self = this;
        let perm = Services.perms.testExactPermissionFromPrincipal(aWindow.document.nodePrincipal, 'engmode-extension');
        this._hasPrivileges = perm == Ci.nsIPermissionManager.ALLOW_ACTION;
        if (!this._hasPrivileges) {
            Cu.reportError('NO ENGMODE EXTENSION PERMISSION ' + 'FOR: ' + aWindow.document.nodePrincipal.origin + '\n');
            return undefined;
        }
        this._window = aWindow;
        this.initDOMRequestHelper(aWindow, ['EngmodeSrv:Common:Return', 'EngmodeSrv:KeyEvent:Return', 'EngmodeSrv:HeadsetStatusChangedEvt:Return', 'EngmodeSrv:RunProcess:Return', 'EngmodeSrv:CopyFile:Return', 'EngmodeSrv:GetFilesLastTime:Return', 'EngmodeSrv:CreateDirOrFile:Return', 'EngmodeSrv:UniversalCommand:Return', 'EngmodeSrv:CheckIsCommandRunnig:Return', 'EngmodeSrv:ReadNvitemRoot:Return', 'EngmodeSrv:ReadNvitem:Return', 'EngmodeSrv:WriteNvitem:Return', 'EngmodeSrv:ReadNvitemEx:Return', 'EngmodeSrv:WriteNvitemEx:Return', 'EngmodeSrv:FileWrite:Return', 'EngmodeSrv:SetDirProp:Return', 'EngmodeSrv:EraseAPN:Return', 'EngmodeSrv:setDataProfileByType:return', 'EngmodeSrv:setInitAttachProfile:return', 'EngmodeSrv:ReadDebugInfo:Return', 'EngmodeSrv:EnableBand41TxASDiv:Return', 'EngmodeSrv:IsBand41TxASDivEnabled:Return', 'EngmodeSrv:SetDdtmStatus:Return', 'EngmodeSrv:GetDdtmStatus:Return', 'EngmodeSrv:SetPropertyValue:Return', 'EngmodeSrv:GetFusest:Return']);
        this._onKeyEvent = null;
        this._requestInfo = {};
    },
    get _currentDateTime() {
        let tempDate = new Date();
        let date_str = tempDate.toLocaleFormat('%Y%m%d_%H%M%S');
        return date_str;
    },
    _getRunStr: function(aProcessType, aStoreToDevice) {
        let run_str;
        switch (aProcessType) {
            case 'bugreport':
                run_str = '/system/bin/bugreport';
                break;
            case 'qxdm':
                run_str = '/system/bin/diag_mdlog -f /vendor/diag.cfg';
                break;
            case 'logcat':
                run_str = '/system/bin/logcat ' + '-b main -b system -b radio -b events -v time';
                break;
            case 'dmesg':
                run_str = '/system/bin/cat /proc/kmsg';
                break;
            case 'tcpdump':
                run_str = '/system/xbin/tcpdump -i any -p -vv -s 0 -w ';
                break;
            default:
                debug('_getRunStr type error' + aProcessType);
                break;
        }
        debug('_getRunStr run_str: ' + run_str);
        return run_str;
    },
    _getDestDicPath: function(aStoreToDevice, aProcessType) {
        let directoryPath;
        if (aStoreToDevice === 'INTERNAL_SDCARD') {
            directoryPath = '/data/usbmsc_mnt/logmanager/' + aProcessType + '/';
        } else {
            directoryPath = '/sdcard/logmanager/' + aProcessType + '/';
        }
        debug('_getDestDicPath  directoryPath: ' + directoryPath);
        return directoryPath;
    },
    _getDestWholePath: function(aStoreToDevice, aProcessType) {
        let destStr = this._getDestDicPath(aStoreToDevice, aProcessType);
        if ('tcpdump' == aProcessType) {
            destStr += aProcessType + '_' + this._currentDateTime + '.pcap';
        } else {
            destStr += aProcessType + '_' + this._currentDateTime + '.log';
        }
        debug('_getDestWholePath  destStr: ' + destStr);
        return destStr;
    },
    _filesLastTimeMsgHdlr: function(aMsg) {
        let obj = aMsg.data;
        let request = this.takeRequest(aMsg.requestID);
        let data = {};
        for (let typeStr in obj) {
            data[typeStr] = {};
            if (obj[typeStr].time !== 0) {
                let tempDate = new Date(obj[typeStr].time);
                let timeStr = tempDate.toLocaleFormat('%Y-%m-%d,%H:%M');
                data[typeStr].timeStr = timeStr;
                data[typeStr].location = obj[typeStr].location;
            } else {
                data[typeStr].timeStr = '';
                data[typeStr].location = '';
            }
        }
        debug('_filesLastTimeMsgHdlr send data: ' + JSON.stringify(data));
        Services.DOMRequest.fireSuccess(request, exposeReadOnly(data));
        delete this._requestInfo[aMsg.requestID];
    },
    _createFileMsgHdlr: function(aMsg) {
        debug('_createFileMsgHdlr' + JSON.stringify(this._requestInfo));
        for (let requestId in this._requestInfo) {
            if (requestId === aMsg.requestID) {
                if (this._requestInfo[requestId].type === 'check') {
                    debug('_createFileMsgHdlr send data: ' + JSON.stringify(aMsg.data));
                    let request = this.takeRequest(aMsg.requestID);
                    Services.DOMRequest.fireSuccess(request, exposeReadOnly(aMsg.data));
                    delete this._requestInfo[aMsg.requestID];
                } else if (aMsg.data === 'EXIST' || aMsg.data === 'CREATE_SUCCESS') {
                    if (this._requestInfo[requestId].type === 'tombstones') {
                        this._copyTombstones(aMsg.requestID);
                    } else if (this._requestInfo[requestId].type === 'crashlog') {
                        this._copyCrashlog(aMsg.requestID);
                    } else if (this._requestInfo[requestId].type === 'qxdm') {
                        this._copyDiagCfg(aMsg.requestID);
                    } else if (this._requestInfo[requestId].type === 'CREATE_SIGNAL') {
                        debug('CREATE_SIGNAL');
                    } else {
                        this._runProcess(aMsg.requestID);
                    }
                } else {
                    debug('_createFileMsgHdlr send data: ' + aMsg.data);
                    let request = this.takeRequest(aMsg.requestID);
                    let data = {};
                    data.type = this._requestInfo[aMsg.requestID].type;
                    data['errorInfo'] = 'Directory or file not exist or create failed';
                    Services.DOMRequest.fireError(request, JSON.stringify(data));
                    delete this._requestInfo[aMsg.requestID];
                }
                return;
            }
        }
    },
    _processMsgHdlr: function(aMessage) {
        let aMsg = aMessage.data;
        if (aMsg.requestID) {
            let aProcessState = aMsg.data.processState;
            for (let requestId in this._requestInfo) {
                if (requestId === aMsg.requestID) {
                    let type = this._requestInfo[requestId].type;
                    debug('processState:' + aProcessState + '  type: ' + type);
                    if (aProcessState === 'process-finished') {
                        if ('qxdm' === type && 'EngmodeSrv:CopyFile:Return' === aMessage.name) {
                            this._runProcess(aMsg.requestID);
                        } else {
                            let aFilePathArray = [];
                            aFilePathArray[0] = this._getDestDicPath('INTERNAL_SDCARD', type) + type + '*';
                            aFilePathArray[1] = this._getDestDicPath('EXTERNAL_SDCARD', type) + type + '*';
                            cpmm.sendAsyncMessage('EngmodeSrv:GetFilesLastTime', {
                                aFilePathArray: aFilePathArray,
                                requestID: aMsg.requestID
                            });
                        }
                    } else if (aProcessState === 'process-running') {

                        if (type === 'qxdm' || type === 'logcat' || type === 'dmesg' || type === 'tcpdump') {
                            let request = this.getRequest(aMsg.requestID);
                            let data = {};
                            data.type = type;
                            data.info = 'running';
                            Services.DOMRequest.fireSuccess(request, exposeReadOnly(data));
                        }
                    } else {
                        let request = this.takeRequest(aMsg.requestID);
                        let data = {};
                        data.type = type;
                        data['errorInfo'] = 'Run process failed';
                        data['errorType'] = aMsg.data.errorType;
                        debug('process-failed: ' + JSON.stringify(data));
                        Services.DOMRequest.fireError(request, JSON.stringify(data));
                        delete this._requestInfo[aMsg.requestID];
                    }
                    return;
                }
            }
        }
    },
    settings: null,
    timeoutForFree: null,
    readJson: function readJson(settingName) {
        if (this.settings == null) {
            let settingsFile = FileUtils.getDir('DefRt', ['settings.json'], false, false);
            if (!settingsFile || (settingsFile && !settingsFile.exists())) {
                return undefined;
            }
            let chan = NetUtil.newChannel(settingsFile);
            let stream = chan.open();
            let converter = Cc['@mozilla.org/intl/scriptableunicodeconverter'].createInstance(Ci.nsIScriptableUnicodeConverter);
            converter.charset = 'UTF-8';
            let rawstr = converter.ConvertToUnicode(NetUtil.readInputStreamToString(stream, stream.available()) || '');
            try {
                this.settings = JSON.parse(rawstr);
            } catch (e) {
                debug('Error parsing ' + settingsFile.path + ' : ' + e);
            }
            stream.close();
        }
        if (this.timeoutForFree) {
            clearTimeout(this.timeoutForFree);
            this.timeoutForFree = null
        }
        var self = this;
        this.timeoutForFree = setTimeout(function() {
            self.settings = null;
        }, 5000);
        return this.settings[settingName];
    },
    filereadwhitelist: {
        'System: ': '/system/system.ver',
        'Boot: ': '/boot.ver',
        'Data: ': '/data/userdata.ver',
        'Recovery: ': '/data/recovery.ver',
        'Modem: ': '/data/modem.ver',
        'SBL1: ': '/proc/modem_sbl',
        'TZ: ': '/proc/modem_tz',
        'RPM: ': '/proc/modem_rpm',
        'Cmdline: ': '/proc/cmdline',
        'Secro: ': '/proc/secro',
        'Study: ': '/data/study.ver',
        'Custpack: ': '/custpack/custpack.ver',
        'Security: ': '/data/simlock.ver',
        'Tuning: ': '/proc/tuning',
        'fumoDLPartitionFile': '/data/fumo_dfInfo',
        'batterycapacity': '/sys/class/power_supply/battery/capacity',
        'health': '/sys/class/power_supply/battery/health',
        'plug': '/sys/class/power_supply/usb/online',
        'batterycurrent_now': '/sys/class/power_supply/battery/current_now',
        'batteryvoltage_now': '/sys/class/power_supply/battery/voltage_now',
        'battery_present': '/sys/class/power_supply/battery/present',
        'battery_id': '/sys/class/power_supply/battery/batt_id',
        'inhotlink': '/storage/sdcard/hotlink/hotlink.json',
        'exhotlink': '/storage/sdcard1/hotlink/hotlink.json',
        'nfcInfo': '/data/nfc/test_result',
        'test_pn547': '/data/testbox_log/test_pn547.txt',
        'GPSif': '/data/testbox_log/gps_info.txt',
        'SubLcdBackLight': '/sys/class/ktd20xx/ktd2026/back_light_led',
        'FlipState': '/sys/go_flip_key/lib_value'
    },
    fileReadLE: function(_file) {
        debug('fileReadLE: ' + this.filereadwhitelist[_file] + '\n');
        var mTemp = this._isCurrentCommand(this.filereadwhitelist, _file);
        if (true == mTemp) {
            let file = Cc['@mozilla.org/file/local;1'].createInstance(Ci.nsILocalFile);
            file.initWithPath(this.filereadwhitelist[_file]);
            let fstream = Cc['@mozilla.org/network/file-input-stream;1'].createInstance(Ci.nsIFileInputStream);
            fstream.init(file, -1, 0, Ci.nsIFileInputStream.DEFER_OPEN);
            let cstream = Cc['@mozilla.org/intl/converter-input-stream;1'].createInstance(Ci.nsIConverterInputStream);
            cstream.init(fstream, 'UTF-8', 0, 0);
            let string = {};
            cstream.readString(-1, string);
            cstream.close();
            fstream.close();
            debug('fileReadLE success: ' + string.value);
            return string.value;
        } else {
            debug('fileReadLE error: error parameter');
            return 'error parameter';
        }
    },
    fileWriteLE: function(str, path, parameter) {
        debug('_fileWriteLE path = ' + path);
        let request = this.createRequest();
        let request_id = this.getRequestId(request);
        var mTemp = this._isCurrentPathtoAllow(path);
        if (true == mTemp) {
            cpmm.sendAsyncMessage('EngmodeSrv:FileWrite', {
                str: str,
                path: path,
                par: parameter,
                requestID: request_id
            });
            this._requestInfo[request_id] = {};
            this._requestInfo[request_id].str = str;
            this._requestInfo[request_id].path = path;
            this._requestInfo[request_id].par = parameter;
        }
        return request;
    },
    getSysInfo: function(aParam) {
        debug('getSysInfo: ' + JSON.stringify(aParam));
        let request = this.createRequest();
        cpmm.sendAsyncMessage('EngmodeSrv:Common', {
            type: aParam,
            requestID: this.getRequestId(request)
        });
        return request;
    },
    _copyTombstones: function(requestId) {
        debug('_copyTombstones: ' +
            JSON.stringify(this._requestInfo[requestId].type));
        let sourcePath = '/data/tombstones';
        let aParam = this._requestInfo[requestId].type;
        let directoryPath = this._getDestDicPath(this._requestInfo[requestId].storeToDevice, this._requestInfo[requestId].type);
        let fileName = aParam + '_' + this._currentDateTime + '.log';
        cpmm.sendAsyncMessage('EngmodeSrv:CopyFile', {
            sourcePath: sourcePath,
            directoryPath: directoryPath,
            fileNewName: fileName,
            requestID: requestId
        });
    },
    _copyCrashlog: function(requestId) {
        debug('_copyCrashlog: ' +
            JSON.stringify(this._requestInfo[requestId].type));
        let sourcePath = '/data/b2g/mozilla/Crash Reports';
        let aParam = this._requestInfo[requestId].type;
        let directoryPath = this._getDestDicPath(this._requestInfo[requestId].storeToDevice, this._requestInfo[requestId].type);
        let fileName = aParam + '_' + this._currentDateTime + '.log';
        cpmm.sendAsyncMessage('EngmodeSrv:CopyFile', {
            sourcePath: sourcePath,
            directoryPath: directoryPath,
            fileNewName: fileName,
            requestID: requestId
        });
    },
    _copyDiagCfg: function(requestId) {
        debug('_copyDiagCfg: ' +
            JSON.stringify(this._requestInfo[requestId].type));
        let sourcePath = '/system/vendor/diag.cfg';
        let directoryPath = this._getDestDicPath(this._requestInfo[requestId].storeToDevice, this._requestInfo[requestId].type);
        let fileName = null;
        cpmm.sendAsyncMessage('EngmodeSrv:CopyFile', {
            sourcePath: sourcePath,
            directoryPath: directoryPath,
            fileNewName: fileName,
            requestID: requestId
        });
    },
    _runProcess: function(requestId) {
        let type = this._requestInfo[requestId].type;
        let aStoreToDevice = this._requestInfo[requestId].storeToDevice;
        debug('_runProcess: ' + JSON.stringify(type) + '  store_to_device: ' + JSON.stringify(aStoreToDevice));
        let startRunStr = 'start ' + this._getRunStr(type, aStoreToDevice);
        let dest_str = this._getDestWholePath(aStoreToDevice, type);
        if ('qxdm' === type) {
            startRunStr += (' -o ' + dest_str);
        } else if ('tcpdump' === type) {
            startRunStr += (' ' + dest_str);
        } else {
            startRunStr += (' >' + dest_str);
        }
        cpmm.sendAsyncMessage('EngmodeSrv:RunProcess', {
            command: startRunStr,
            requestID: requestId
        });
    },
    _isLegalPath: function(aProcessType) {
        switch (aProcessType) {
            case 'tombstones':
            case 'bugreport':
            case 'qxdm':
            case 'dmesg':
            case 'logcat':
            case 'crashlog':
            case 'initlog':
            case 'geckolog':
            case 'tcpdump':
            case 'settingsapnvalue':
            case 'versioninfo':
            case 'clearlogs':
            case 'propvalue':
            case 'nvvalue':
                return true;
                break;
            default:
                return false;
                break;
        }
    },
    createFileLE: function(typestr, path) {
        debug('createnewDirorFile: ' + ' typestr: ' + typestr + ' path: ' + path);
        let request = this.createRequest();
        let request_id = this.getRequestId(request);
        this._requestInfo[request_id] = {};
        this._requestInfo[request_id].typestr = typestr;
        this._requestInfo[request_id].path = path;
        if (path.indexOf('/sdcard') !== 0) {
            cpmm.sendAsyncMessage('EngmodeSrv:CreateDirOrFile', {
                path: path,
                typeStr: typestr,
                shouldCreate: true,
                requestID: request_id
            });
            this._requestInfo[request_id].storeToDevice = 'INTERNAL_SDCARD';
        } else {
            cpmm.sendAsyncMessage('EngmodeSrv:CreateDirOrFile', {
                path: path,
                typeStr: typestr,
                shouldCreate: true,
                requestID: request_id
            });
            this._requestInfo[request_id].storeToDevice = 'EXTERNAL_SDCARD';
        }
        if (path.indexOf('tombstones') > 0) {
            this._requestInfo[request_id].type = 'tombstones';
        } else if (path.indexOf('crashlog') > 0) {
            this._requestInfo[request_id].type = 'crashlog';
        } else if (path.indexOf('bugreport') > 0) {
            this._requestInfo[request_id].type = 'bugreport';
        } else if (path.indexOf('dmesg') > 0) {
            this._requestInfo[request_id].type = 'dmesg';
        } else {
            this._requestInfo[request_id].type = 'CREATE_SIGNAL';
        }
        return request;
    },
    run: function(aParam, aStoreToDevice) {
        debug('run: ' + ' aParam: ' + aParam + ' aStoreToDevice: ' + aStoreToDevice);
        let request = this.createRequest();
        let request_id = this.getRequestId(request);
        if (this._isLegalPath(aParam)) {
            let path = this._getDestDicPath(aStoreToDevice, aParam);
            cpmm.sendAsyncMessage('EngmodeSrv:CreateDirOrFile', {
                path: path,
                typeStr: 'DIRECTORY',
                shouldCreate: true,
                requestID: request_id
            });
            this._requestInfo[request_id] = {};
            this._requestInfo[request_id].type = aParam;
            this._requestInfo[request_id].storeToDevice = aStoreToDevice;
        } else {
            let data = {};
            data.type = aParam;
            data['errorInfo'] = 'Store log to a wrong place';
            data['errorType'] = 'IllegalFilePathToStoreLog';
            debug('IllegalFilePathToStoreLog' + JSON.stringify(data));
            Services.DOMRequest.fireErrorAsync(request, JSON.stringify(data));
        }
        return request;
    },
    stop: function(aParam) {
        debug('stop: ' + JSON.stringify(aParam));
        let request = this.createRequest();
        let request_id = this.getRequestId(request);
        let runStr = this._getRunStr(aParam);
        let stopRunStr = 'stop ' + runStr;
        if (aParam === 'qxdm') {
            stopRunStr = 'start ' + '/system/bin/diag_mdlog -k';
        }
        cpmm.sendAsyncMessage('EngmodeSrv:RunProcess', {
            command: stopRunStr,
            requestID: request_id
        });
        this._requestInfo[request_id] = {};
        this._requestInfo[request_id].type = aParam;
        return request;
    },

    startAudioLoopTest: function(aParam) {
        debug('mmitest--startAudioLoopTest: ' + aParam);
        cpmm.sendAsyncMessage('EngmodeSrv:AudioLoop', {
            param: aParam,
            operation: 'start'
        });
    },
    stopAudioLoopTest: function() {
        debug('mmitest--stopAudioLoopTest.');
        cpmm.sendAsyncMessage('EngmodeSrv:AudioLoop', {
            param: 'mic',
            operation: 'stop'
        });
    },
    startForceInCall: function() {
        cpmm.sendAsyncMessage('EngmodeSrv:ForceInCall', {
            operation: 'start'
        });
    },
    stopForceInCall: function() {
        cpmm.sendAsyncMessage('EngmodeSrv:ForceInCall', {
            operation: 'stop'
        });
    },
    startUniversalCommand: function(command, isUseShell) {
        let request = this.createRequest();
        cpmm.sendAsyncMessage('EngmodeSrv:UniversalCommand', {
            param: command,
            useShell: isUseShell,
            operation: 'start',
            requestID: this.getRequestId(request)
        });
        return request;
    },
    startUniversalCommandPre: function(command, isUseShell, type) {
        debug('startUniversalCommandPre ' +
            command + ' ' + isUseShell + ' ' + type);
        let runStr = this._getRunStr(type);
        let runStrArgList = runStr.split(' ');
        debug('runStrArgList ' + runStrArgList);
        if (runStrArgList[0]) {
            command = command + ' ' + runStrArgList[0];
            debug('command ' + command);
            debug('result ' + runStrArgList[0]);
        }
        this.startUniversalCommand(command, isUseShell);
    },
    stopUniversalCommand: function() {
        cpmm.sendAsyncMessage('EngmodeSrv:UniversalCommand', {
            operation: 'stop'
        });
    },
    startGpsTest: function() {
        cpmm.sendAsyncMessage('EngmodeSrv:GpsTest', {
            operation: 'start'
        });
    },
    stopGpsTest: function() {
        cpmm.sendAsyncMessage('EngmodeSrv:GpsTest', {
            operation: 'stop'
        });
    },
    setChargerLed: function(config) {
        debug('SetChargerLed');
        cpmm.sendAsyncMessage('EngmodeSrv:SetChargerLED', {
            config: config
        });
    },
    setCameraLed: function(config) {
        debug('SetCameraLed');
        cpmm.sendAsyncMessage('EngmodeSrv:SetCameraLED', {
            config: config
        });
    },
    setSubLCD: function(config) {
        debug('setSubLCD');
        cpmm.sendAsyncMessage('EngmodeSrv:setSubLCD', {
            config: config
        });
    },
    setKeypadLED: function(config) {
        debug('setKeypadLED');
        cpmm.sendAsyncMessage('EngmodeSrv:setKeypadLED', {
            config: config
        });
    },
    setChargingEnabled: function(config) {
        debug('SetChargingEnabled');
        cpmm.sendAsyncMessage('EngmodeSrv:SetChargingEnabled', {
            config: config
        });
    },
    setDbClickUnlock: function(config) {
        debug('SetDbClickUnlock');
        cpmm.sendAsyncMessage('EngmodeSrv:SetDbClickUnlock', {
            config: config
        });
    },
    readNvitem: function(item) {
        debug('EngmodeExtension readNvitem');
        let request = this.createRequest();
        cpmm.sendAsyncMessage('EngmodeSrv:ReadNvitem', {
            item: item,
            requestID: this.getRequestId(request)
        });
        return request;
    },
    writeNvitem: function(item, length, value) {
        debug('EngmodeExtension writeNvitem');
        let request = this.createRequest();
        cpmm.sendAsyncMessage('EngmodeSrv:WriteNvitem', {
            item: item,
            length: length,
            value: value,
            requestID: this.getRequestId(request)
        });
        return request;
    },
    readNvitemRoot: function(item) {
        debug('EngmodeExtension readNvitemRoot');
        let request = this.createRequest();
        cpmm.sendAsyncMessage('EngmodeSrv:ReadNvitemRoot', {
            item: item,
            requestID: this.getRequestId(request)
        });
        return request;
    },
    setDirProp: function(path, status) {
        debug('EngmodeExtension setDirProp');
        let request = this.createRequest();
        cpmm.sendAsyncMessage('EngmodeSrv:SetDirProp', {
            path: path,
            status: status,
            requestID: this.getRequestId(request)
        });
        return request;
    },
    eraseAPN: function(apnno) {
        debug('EngmodeExtension eraseAPN');
        let request = this.createRequest();
        cpmm.sendAsyncMessage('EngmodeSrv:EraseAPN', {
            apnno: apnno,
            requestID: this.getRequestId(request)
        });
        return request;
    },
    enableBand41TxASDiv: function(enable) {
        debug('EngmodeExtension enableBand41TxASDiv');
        let request = this.createRequest();
        cpmm.sendAsyncMessage('EngmodeSrv:EnableBand41TxASDiv', {
            enalbe: enable,
            requestID: this.getRequestId(request)
        });
        return request;
    },
    isBand41TxASDivEnabled: function() {
        debug('EngmodeExtension enableBand41TxASDiv');
        let request = this.createRequest();
        cpmm.sendAsyncMessage('EngmodeSrv:IsBand41TxASDivEnabled', {
            requestID: this.getRequestId(request)
        });
        return request;
    },
    RemoveBandFromPriorityList: function(band) {
        debug('EngmodeExtension RemoveBandFromPriorityList');
        let request = this.createRequest();
        cpmm.sendAsyncMessage('EngmodeSrv:RemoveBandFromPriorityList', {
            band: band,
            requestID: this.getRequestId(request)
        });
        return request;
    },
    AddBandToPriorityList: function(band) {
        debug('EngmodeExtension AddBandToPriorityList');
        let request = this.createRequest();
        cpmm.sendAsyncMessage('EngmodeSrv:AddBandToPriorityList', {
            band: band,
            requestID: this.getRequestId(request)
        });
        return request;
    },
    setInitAttachProfile: function(strApnSettings) {
        debug('EngmodeExtension cxk setInitAttachProfile');
        let request = this.createRequest();
        let apnSettings = JSON.parse(strApnSettings);
        apnSettings.protocol = apnSettings.protocol.toUpperCase();
        cpmm.sendAsyncMessage('EngmodeSrv:setInitAttachProfile', {
            apn: apnSettings.apn,
            protocol: apnSettings.protocol,
            authtype: apnSettings.authtype,
            user: apnSettings.user,
            password: apnSettings.password,
            requestID: this.getRequestId(request)
        });
        return request;
    },
    setDataProfileByType: function(strApnSettings, type) {
        debug('EngmodeExtension setDataProfileByType');
        let request = this.createRequest();
        let apnSettings = JSON.parse(strApnSettings);
        apnSettings.protocol = apnSettings.protocol.toUpperCase();
        apnSettings.enabled = (apnSettings.enabled === 'true' ? 1 : 0);
        for (let i = 0; apnSettings.types[i]; i++) {
            debug('apnSettings.type index:' + i);
            if (apnSettings.types[i] == 'default') {
                apnSettings.profileId = INET_CLASS;
                apnSettings.networkType = NETWORK_TYPE_MOBILE;
                break;
            } else if (apnSettings.types[i] == 'supl') {
                apnSettings.profileId = APP_CLASS;
                apnSettings.networkType = NETWORK_TYPE_MOBILE_SUPL;
                break;
            } else if (apnSettings.types[i] == 'ims') {
                apnSettings.profileId = IMS_CLASS;
                apnSettings.networkType = NETWORK_TYPE_MOBILE_IMS;
                break;
            } else if (apnSettings.types[i] == 'dun') {
                apnSettings.profileId = ADMN_CLASS;
                apnSettings.networkType = NETWORK_TYPE_MOBILE_DUN;
                break;
            }
        }
        cpmm.sendAsyncMessage('EngmodeSrv:setDataProfileByType', {
            networkType: apnSettings.networkType,
            profileId: apnSettings.profileId,
            apn: apnSettings.apn,
            protocol: apnSettings.protocol,
            authtype: apnSettings.authtype,
            user: apnSettings.user,
            password: apnSettings.password,
            types: type,
            maxConnsTime: apnSettings.maxConnsTime,
            maxConns: apnSettings.maxConns,
            waitTime: apnSettings.waitTime,
            enabled: apnSettings.enabled,
            inactivityTimer: apnSettings.inactivityTimer,
            requestID: this.getRequestId(request)
        });
        return request;
    },
    readNvitemEx: function(item) {
        debug('EngmodeExtension readNvitemEx');
        let request = this.createRequest();
        cpmm.sendAsyncMessage('EngmodeSrv:ReadNvitemEx', {
            item: item,
            requestID: this.getRequestId(request)
        });
        return request;
    },
    writeNvitemEx: function(item, length, value) {
        debug('EngmodeExtension writeNvitemEx');
        let request = this.createRequest();
        cpmm.sendAsyncMessage('EngmodeSrv:WriteNvitemEx', {
            item: item,
            length: length,
            value: value,
            requestID: this.getRequestId(request)
        });
        return request;
    },
    readDebugInfo: function(item) {
        debug('EngmodeExtension readDebugInfo');
        let request = this.createRequest();
        cpmm.sendAsyncMessage('EngmodeSrv:ReadDebugInfo', {
            item: item,
            requestID: this.getRequestId(request)
        });
        return request;
    },
    setDdtmStatus: function(enable, appName) {
        debug('EngmodeExtension setDdtmStatus');
        let request = this.createRequest();
        cpmm.sendAsyncMessage('EngmodeSrv:SetDdtmStatus', {
            enable: enable,
            appName: appName,
            requestID: this.getRequestId(request)
        });
        return request;
    },
    getDdtmStatus: function() {
        debug('EngmodeExtension getDdtmStatus');
        let request = this.createRequest();
        cpmm.sendAsyncMessage('EngmodeSrv:GetDdtmStatus', {
            requestID: this.getRequestId(request)
        });
        return request;
    },
    getFilesLastTime: function(type, aParamAarry, aLength) {
        debug('getFilesLastTime: type = ' + type);
        debug('getFilesLastTime: aParamAarry = ' + aParamAarry.toString());
        debug('getFilesLastTime: aLength = ' + aLength);
        let request = this.createRequest();
        let aFilePathArray = [];
        if ('default' == type) {
            for (let i = 0; i < aParamAarry.length; i++) {
                aFilePathArray[aFilePathArray.length] = this._getDestDicPath('INTERNAL_SDCARD', aParamAarry[i]) + aParamAarry[i] + '*';
                aFilePathArray[aFilePathArray.length] = this._getDestDicPath('EXTERNAL_SDCARD', aParamAarry[i]) + aParamAarry[i] + '*';
            }
        } else {
            for (let i = 0; i < aParamAarry.length; i++) {
                aFilePathArray[aFilePathArray.length] = aParamAarry[i];
            }
        }
        cpmm.sendAsyncMessage('EngmodeSrv:GetFilesLastTime', {
            aFilePathArray: aFilePathArray,
            requestID: this.getRequestId(request)
        });
        return request;
    },
    checkIsFileExist: function(filePath) {
        debug('checkIsFileExist: ' + ' filePath: ' + filePath);
        let request = this.createRequest();
        let request_id = this.getRequestId(request);
        cpmm.sendAsyncMessage('EngmodeSrv:CreateDirOrFile', {
            path: filePath,
            typeStr: 'FILE',
            shouldCreate: false,
            requestID: request_id
        });
        this._requestInfo[request_id] = {};
        this._requestInfo[request_id].type = 'check';
        return request;
    },
    checkIsCommandRunning: function(aParam) {
        debug('checkIsCommandRunning: ' + ' aParam: ' + aParam);
        let pattern = /(\S+)\s+([^>]*)(?:>\s*(\S+))?/;
        let result;
        let runStr = this._getRunStr(aParam);
        if (result = pattern.exec(runStr)) {
            let program = result[1];
            let request = this.createRequest();
            let request_id = this.getRequestId(request);
            cpmm.sendAsyncMessage('EngmodeSrv:CheckIsCommandRunnig', {
                commands: program,
                requestID: request_id
            });
            return request;
        }
        return null;
    },
    set onkeyevent(keyEvtHandle) {
        debug('onkeyevent');
        if (this._onKeyEvent === keyEvtHandle) {
            debug('onkeyevent: redundant setting...');
        } else if (this._onKeyEvent && keyEvtHandle) {
            debug('onkeyevent: key handler changed');
            this._onKeyEvent = keyEvtHandler;
        } else if (this._onKeyEvent === null) {
            debug('onkeyevent: key handler set');
            this._onKeyEvent = keyEvtHandle;
            cpmm.sendAsyncMessage('EngmodeSrv:HookKeyEvt', {
                enable: true
            });
        } else {
            debug('onkeyevent: key handler unset');
            this._onKeyEvent = null;
            cpmm.sendAsyncMessage('EngmodeSrv:HookKeyEvt', {
                enable: false
            });
        }
    },
    set onHeadsetStatusChanged(headsetStatusChangedEvtHandle) {
        if (headsetStatusChangedEvtHandle) {
            this._onHeadsetStatusChanged = headsetStatusChangedEvtHandle;
            cpmm.sendAsyncMessage('EngmodeSrv:HookHeadsetStatusChangedEvt', {
                enable: true
            });
        } else {
            this._onHeadsetStatusChanged = null;
            cpmm.sendAsyncMessage('EngmodeSrv:HookHeadsetStatusChangedEvt', {
                enable: false
            });
        }
    },
    getFlipState: function() {
        let value = null;
        value = this.fileReadLE('FlipState');
        if (value == 0) {
            value = 'open';
        } else if (value == 1) {
            value = 'close';
        }
        return value;
    },
    _isCurrentCommand: function(list, keys) {
        for (var i in list) {
            if (i == keys) {
                debug('_isCurrentCommand: keys = ' + keys);
                return true;
            }
        }
        return false;
    },
    _isCurrentPathtoAllow: function(path) {
        if (1 <= path.length) {
            debug(' _isCurrentPathtoAllow: path = ' + path);
            if (('/storage/sdcard' == path.substr(0, 15)) || ('/data/kaioslog' == path.substr(0, 14)) || ('/data/logmanager' == path.substr(0, 16)) || ('/data/testbox_log' == path.substr(0, 17)) || ('/system/system.ver' == path) || ('/proc/study' == path) || ('/data/userdata.ver' == path) || ('/system/b2g/defaults/pref/user.js' == path) || ('/data/nfc_pcd.txt' == path)) {
                return true;
            }
        }
        return false;
    },
    propsWhiteList: {
        'mms_debugging': 'mms.debugging.enabled',
        'kaios_diagcfg': 'persist.sys.kaiosdiagcfg.enable',
        'kaios_initkeys': 'persist.sys.initkeys.enable',
        'settings_console': 'debug.console.enabled',
        'settings_wifidebug': 'wifi.debugging.enabled',
        'bg_service': 'persist.sys.engmodeexlog.enable',
        'calling_autoanswer': 'persist.sys.tel.autoanswer.ms',
        'calling_autoGPRS': 'persist.ro.ril.data_auto_attach',
        'relay_oprt_change': 'persist.radio.relay_oprt_change',
        'auto_enable': 'persist.sys.autolog.enable',
        'auto_size': 'persist.sys.autolog.props',
        'settings_HAC': 'persist.HACSetting',
        'settings_TTY': 'persist.tty_mode',
        'oma_AauthData': 'persist.oma.aauthdata'
    },
    setPropertyLE: function(propskey, value) {
        debug('setPropertyLE: propskey = ' +
            this.propsWhiteList[propskey] + 'value = ' + value);
        let request = this.createRequest();
        var iscurComm = this._isCurrentCommand(this.propsWhiteList, propskey);
        if (true == iscurComm) {
            var command = 'setprop ' + this.propsWhiteList[propskey] + ' ' + value;
            debug('setPropertyLE: command = ' + command);
            cpmm.sendAsyncMessage('EngmodeSrv:UniversalCommand', {
                param: command,
                useShell: true,
                operation: 'start',
                requestID: this.getRequestId(request)
            });
        }
        return request;
    },
    commandspecifyList: {
        'data_nfc_upper': 'mkdir -p /data/nfc; touch /data/nfc/test_result; chmod -R 775 /data/nfc',
        'data_kaioslog_upper': 'chmod -R 755 /data/kaioslog',
        'data_testbox_upper': 'chmod -R 755 /data/testbox_log',
        'data_pidpptt_upper': 'chmod 0774 /data/misc/wifi/pid.ptt',
        'kaiosreboot': 'reboot',
        'delete_kaioslog_file': 'rm -r',
        'wifitest': '/system/bin/wifitest',
        'bt_radio_run': '/system/bin/bt_radio_run',
        'bt_radio_stop': '/system/bin/bt_radio_stop',
        'btTx_start': '/system/bin/btTx_start',
        'btTx_run': '/system/bin/btTx_run',
        'nfc_test': 'if [ -f /system/bin/pnscr ]; then \n\
                   if [ -e /dev/nq-nci ]; then \n\
                     /system/bin/pnscr -t 1 -d nq-nci -f /system/etc/nfc/Card_Mode.txt > /data/nfc/test_result \n\
                   elif [ -e /dev/pn544 ]; then \n\
                     /system/bin/pnscr -t 1 -d pn544 -f /system/etc/nfc/Card_Mode.txt > /data/nfc/test_result \n\
                   fi \n\
                 elif [ -f /system/bin/secnfc ]; then \n\
                   /system/bin/secnfc > /data/nfc/test_result \n\
                 fi',
        'nfc_stop': 'test -f /data/nfc/test_result && rm -f /data/nfc/test_result \n\
                 if [ -f /system/bin/pnscr ]; then \n\
                   /system/bin/pkill -9 -f pnscr \n\
                 elif [ -f /system/bin/secnfc ]; then \n\
                   /system/bin/pkill -9 -f secnfc \n\
                 fi',
        'bttest': 'bttest',
        'bttestdisable': 'bttest disable',
        'gps_test': '/system/bin/gps_test',
        'backup_sfs': '/system/bin/backup_sfs',
        'gps_test_1_0': '/system/bin/gps_test 1 0',
        'gps_test_0_1': '/system/bin/gps_test 0 1',
        'gps_test_1_0_1': '/system/bin/gps_test 1 0 1',
        'test_pn547': '/system/bin/test_pn547',
        'busybox_tar': 'busybox tar',
        'sublcdtest': '/system/bin/fb_test',
        'rmgps': 'rm -r /data/testbox_log/gps_info.txt',
        'screencap_p': '/system/bin/screencap -p'
    },
    _isCurrentParam: function(aParam, length) {
        if (('>' != aParam.match('>')) && (length > aParam.length)) {
            return true;
        } else {
            return false;
        }
    },
    execCmdLE: function(aParamAarry, aLength) {
        let request = this.createRequest();
        var mTempCommand = aParamAarry[0];
        debug('engmodeExtension execCmdLE: aParamAarry[0] = ' +
            this.commandspecifyList[mTempCommand]);
        if (this._isCurrentCommand(this.commandspecifyList, aParamAarry[0]) && (0 < aParamAarry.length) && (aParamAarry.length <= 3)) {
            var mCurrentParam = this.commandspecifyList[mTempCommand];
            var command = null;
            if ((3 == aLength) && (this._isCurrentParam(aParamAarry[1], 256)) && (this._isCurrentParam(aParamAarry[2], 1024))) {
                var isCurPathAllow = this._isCurrentPathtoAllow(aParamAarry[2]);
                if (true == isCurPathAllow) {
                    command = mCurrentParam + ' ' + aParamAarry[1] + ' > ' + aParamAarry[2];
                }
            } else if ((2 == aLength) && (this._isCurrentParam(aParamAarry[1], 256))) {
                if (('stop' == aParamAarry[1]) && ('gps_test' == aParamAarry[0])) {
                    debug('execCmdLE: command = ' + command);
                    cpmm.sendAsyncMessage('EngmodeSrv:UniversalCommand', {
                        operation: 'stop'
                    });
                    return request;
                } else {
                    command = mCurrentParam + ' ' + aParamAarry[1];
                }
            } else if (1 == aLength) {
                command = mCurrentParam;
            }
            debug('execCmdLE: command = ' + command);
            cpmm.sendAsyncMessage('EngmodeSrv:UniversalCommand', {
                param: command,
                useShell: true,
                operation: 'start',
                requestID: this.getRequestId(request)
            });
            return request;
        }
        return request;
    },
    setKAIOSLogPara: function(keys, aParamAarry, aLength) {
        var command = null;
        if (2 == aLength) {
            if (this._isLegalPath(keys)) {
                debug('setKAIOSLogPara: tempkeys1 = ' + aParamAarry[0] + ' tempkeys2 = ' + aParamAarry[1]);
                var tempkeys1 = aParamAarry[0];
                var tempkeys2 = aParamAarry[1];
                if ('enable' == tempkeys1) {
                    command = 'setprop persist.sys.kaios' + keys + '.enable ' + tempkeys2;
                } else if ('path' == tempkeys1) {
                    command = 'setprop persist.sys.kaios' + keys + '.path ' + tempkeys2;
                } else if ('cmd' == tempkeys1) {
                    command = 'setprop persist.sys.kaios' + keys + '.cmd ' + tempkeys2;
                }
                let request = this.createRequest();
                cpmm.sendAsyncMessage('EngmodeSrv:UniversalCommand', {
                    param: command,
                    useShell: true,
                    operation: 'start',
                    requestID: this.getRequestId(request)
                });
                return request;
            }
        }
        return request;
    },
    receiveMessage: function(aMessage) {
        debug('receiveMessage: ' + JSON.stringify(aMessage));
        let request;
        let msg = aMessage.data;
        switch (aMessage.name) {
            case 'EngmodeSrv:Common:Return':
                request = this.takeRequest(msg.requestID);
                if (request) {
                    if (msg.data.result === 'OK') {
                        debug('Success: ' + JSON.stringify(msg));
                        Services.DOMRequest.fireSuccess(request, exposeReadOnly(msg.data.data));
                    } else if (msg.data.result === 'KO') {
                        debug('Fail: ' + JSON.stringify(msg));
                        let errorInfo;
                        if (typeof msg.data.data === 'object') {
                            errorInfo = JSON.stringify(msg.data.data);
                        } else {
                            errorInfo = msg.data;
                        }
                        debug('Send data:' + errorInfo);
                        Services.DOMRequest.fireError(request, errorInfo);
                    }
                } else {
                    debug('Success: Can not find the request object with ' +
                        msg.requestID);
                }
                break;
            case 'EngmodeSrv:RunProcess:Return':
            case 'EngmodeSrv:CopyFile:Return':
                this._processMsgHdlr(aMessage);
                break;
            case 'EngmodeSrv:GetFilesLastTime:Return':
                this._filesLastTimeMsgHdlr(msg);
                break;
            case 'EngmodeSrv:CreateDirOrFile:Return':
                this._createFileMsgHdlr(msg);
                break;
            case 'EngmodeSrv:CheckIsCommandRunnig:Return':
                request = this.takeRequest(msg.requestID);
                Services.DOMRequest.fireSuccess(request, exposeReadOnly(msg.status));
                break;
            case 'EngmodeSrv:KeyEvent:Return':
                if (this._onKeyEvent !== null) {
                    debug('To process Key event' + JSON.stringify(msg));
                    let event = new this._window.Event(msg.event);
                    this._onKeyEvent.handleEvent(event);
                } else {
                    debug('The key processor is unregistered');
                }
                break;
            case 'EngmodeSrv:HeadsetStatusChangedEvt:Return':
                if (this._onHeadsetStatusChanged !== null) {
                    let event = new this._window.Event(msg.event);
                    this._onHeadsetStatusChanged.handleEvent(event);
                }
                break;
            case 'EngmodeSrv:UniversalCommand:Return':
                request = this.takeRequest(msg.requestID);
                if (request) {
                    if (msg.data === 'process-finished') {
                        Services.DOMRequest.fireSuccess(request, exposeReadOnly(msg.data));
                    } else {
                        Services.DOMRequest.fireError(request, exposeReadOnly(msg.data));
                    }
                } else {
                    debug('Success: Can not find the request object with ' +
                        msg.requestID);
                }
                break;
            case 'EngmodeSrv:ReadNvitemRoot:Return':
                debug('ReadNvitemRoot:' + msg.requestID);
                request = this.takeRequest(msg.requestID);
                if (request) {
                    if (msg.data.result === 'OK') {
                        debug('Success: ' + JSON.stringify(msg));
                        Services.DOMRequest.fireSuccess(request, exposeReadOnly(msg.data.data));
                    } else if (msg.data.result === 'KO') {
                        debug('Fail: ' + JSON.stringify(msg));
                        Services.DOMRequest.fireError(request, msg.data.data);
                    }
                } else {
                    debug('Success: Can not find the request object with ' +
                        msg.requestID);
                }
                break;
            case 'EngmodeSrv:ReadNvitem:Return':
                debug('ReadNvitem:' + msg.requestID);
                request = this.takeRequest(msg.requestID);
                if (request) {
                    if (msg.data.result === 'OK') {
                        debug('Success: ' + JSON.stringify(msg));
                        Services.DOMRequest.fireSuccess(request, Cu.cloneInto(msg.data.data, this._window));
                    } else if (msg.data.result === 'KO') {
                        debug('Fail: ' + JSON.stringify(msg));
                        Services.DOMRequest.fireError(request, msg.data.data);
                    }
                } else {
                    debug('Success: Can not find the request object with ' +
                        msg.requestID);
                }
                break;
            case 'EngmodeSrv:GetFusest:Return':
                debug('OpenFuse:' + msg.result);
                if (this._getOemfuseCb != null) {
                    this._getOemfuseCb(msg.result);
                }
                break;
            case 'EngmodeSrv:WriteNvitem:Return':
                debug('EngmodeSrv:WriteNvitem:Return' + msg.requestID);
                request = this.takeRequest(msg.requestID);
                Services.DOMRequest.fireSuccess(request, Cu.cloneInto(msg.data.data, this_self._window));
                break;
            case 'EngmodeSrv:ReadNvitemEx:Return':
                debug('EngmodeSrv:ReadNvitemEx:Return:' + msg.requestID);
                request = this.takeRequest(msg.requestID);
                if (request) {
                    if (msg.data.result === 'OK') {
                        debug('Success: ' + JSON.stringify(msg));
                        Services.DOMRequest.fireSuccess(request, exposeReadOnly(msg.data.data));
                    } else if (msg.data.result === 'KO') {
                        debug('Fail: ' + JSON.stringify(msg));
                        Services.DOMRequest.fireError(request, msg.data.data);
                    }
                } else {
                    debug('Success: Can not find the request object with ' +
                        msg.requestID);
                }
                break;
            case 'EngmodeSrv:ReadDebugInfo:Return':
                debug('EngmodeSrv:ReadDebugInfo:Return:' + msg.requestID);
                request = this.takeRequest(msg.requestID);
                if (request) {
                    if (msg.data.result === 'OK') {
                        debug('Success: ' + JSON.stringify(msg));
                        Services.DOMRequest.fireSuccess(request, exposeReadOnly(msg.data.data));
                    } else if (msg.data.result === 'KO') {
                        debug('Fail: ' + JSON.stringify(msg));
                        Services.DOMRequest.fireError(request, msg.data.data);
                    }
                } else {
                    debug('Success: Can not find the request object with ' +
                        msg.requestID);
                }
                break;
            case 'EngmodeSrv:SetDdtmStatus:Return':
                debug('EngmodeSrv:SetDdtmStatus:Return:' + msg.requestID);
                request = this.takeRequest(msg.requestID);
                Services.DOMRequest.fireSuccess(request, exposeWrite(msg.data.data));
                break;
            case 'EngmodeSrv:GetDdtmStatus:Return':
                debug('EngmodeSrv:GetDdtmStatus:Return:' + msg.requestID);
                request = this.takeRequest(msg.requestID);
                if (request) {
                    if (msg.data.result === 'OK') {
                        debug('Success: ' + JSON.stringify(msg));
                        Services.DOMRequest.fireSuccess(request, exposeReadOnly(msg.data.data));
                    } else if (msg.data.result === 'KO') {
                        debug('Fail: ' + JSON.stringify(msg));
                        Services.DOMRequest.fireError(request, msg.data.data);
                    }
                } else {
                    debug('Success: Can not find the request object with ' +
                        msg.requestID);
                }
                break;
            case 'EngmodeSrv:WriteNvitemEx:Return':
                debug('EngmodeSrv:WriteNvitemEx:Return' +
                    msg.requestID);
                request = this.takeRequest(msg.requestID);
                Services.DOMRequest.fireSuccess(request, exposeWrite(msg.data.data));
                break;
            case 'EngmodeSrv:SetPropertyValue:Return':
                debug('EngmodeSrv:SetPropertyValue:Return: ' +
                    msg.requestID);
                request = this.takeRequest(msg.requestID);
                if (request) {
                    if (msg.data.result === 'OK') {
                        Services.DOMRequest.fireSuccess(request, null);
                    } else if (msg.data.result === 'KO') {
                        Services.DOMRequest.fireError(request, null);
                    }
                }
                break;
            case 'EngmodeSrv:FileWrite:Return':
                request = this.takeRequest(msg.requestID);
                if (request) {
                    if (msg.data.result === 'OK') {
                        debug('Success: ' + JSON.stringify(msg));
                        Services.DOMRequest.fireSuccess(request, msg.data.data);
                    } else if (msg.data.result === 'KO') {
                        debug('Fail: ' + JSON.stringify(msg));
                        Services.DOMRequest.fireError(request, msg.data.data);
                    }
                } else {
                    debug('Success: Can not find the request object with ' +
                        msg.requestID);
                }
                break;
            case 'EngmodeSrv:EnableBand41TxASDiv:Return':
                request = this.takeRequest(msg.requestID);
                if (msg.data.result === 'OK') {
                    debug('Success: ' + JSON.stringify(msg));
                    Services.DOMRequest.fireSuccess(request, msg.data.data);
                } else {
                    debug('Fail: ' + JSON.stringify(msg));
                    Services.DOMRequest.fireError(request, msg.data.data);
                }
                break;
            case 'EngmodeSrv:IsBand41TxASDivEnabled:Return':
                request = this.takeRequest(msg.requestID);
                if (msg.data.result === 'OK') {
                    debug('Success: ' + JSON.stringify(msg));
                    Services.DOMRequest.fireSuccess(request, msg.data.data);
                } else {
                    debug('Fail: ' + JSON.stringify(msg));
                    Services.DOMRequest.fireError(request, msg.data.data);
                }
                break;
            case 'EngmodeSrv:EraseAPN:Return':
                request = this.takeRequest(msg.requestID);
                if (request) {
                    if (msg.data.result === 'OK') {
                        debug('Success: ' + JSON.stringify(msg));
                        Services.DOMRequest.fireSuccess(request, msg.data.data);
                    } else if (msg.data.result === 'KO') {
                        debug('Fail: ' + JSON.stringify(msg));
                        Services.DOMRequest.fireError(request, msg.data.data);
                    }
                } else {
                    debug('Success: Can not find the request object with ' +
                        msg.requestID);
                }
                break;
            case 'EngmodeSrv:SetDirProp:Return':
                request = this.takeRequest(msg.requestID);
                if (request) {
                    if (msg.data.result === 'OK') {
                        debug('Success: ' + JSON.stringify(msg));
                        Services.DOMRequest.fireSuccess(request, msg.data.data);
                    } else if (msg.data.result === 'KO') {
                        debug('Fail: ' + JSON.stringify(msg));
                        Services.DOMRequest.fireError(request, msg.data.data);
                    }
                } else {
                    debug('Success: Can not find the request object with ' +
                        msg.requestID);
                }
                break;
            case 'EngmodeSrv:setDataProfileByType:return':
                request = this.takeRequest(msg.requestID);
                if (request) {
                    if (msg.data.result === 'OK') {
                        debug('Success: ' + JSON.stringify(msg));
                        Services.DOMRequest.fireSuccess(request, msg.data.data);
                    } else if (msg.data.result === 'KO') {
                        debug('Fail: ' + JSON.stringify(msg));
                        Services.DOMRequest.fireError(request, msg.data.data);
                    }
                } else {
                    debug('Success: Can not find the request object with ' +
                        msg.requestID);
                }
                break;
            default:
                debug('Error Message' + aMessage.name);
                break;
        }
    },
    setPropertyValue: function _SetPropertyValue(propertyName, propertyValue) {
        debug('_SetPropertyValue name: ' + propertyName + ', value: ' + propertyValue);
        cpmm.sendAsyncMessage('EngmodeSrv:SetPropertyValue', {
            name: propertyName,
            value: propertyValue
        });
        return 'true';
    },
    getPropertyValue: function _GetPropertyValue(propertyName) {
        debug('getPropertyValue name is ' + propertyName);
        var value = libcutils.property_get(propertyName);
        return value;
    },
    readRoValue: function(ro) {
        return this.getPropertyValue(ro);
    },
    getPrefValue: function(prefId, defaultVal) {
        let val = defaultVal;
        try {
            val = Services.prefs.getCharPref(prefId);
        } catch (ex) {}
        debug('getPrefValue --prefId(' + prefId + ') = ' + val);
        return val;
    },
    getOemfuseStatus: function(cb) {
        this._getOemfuseCb = cb;
        debug('EngmodeExtension openfuse');
        cpmm.sendAsyncMessage('EngmodeSrv:GetFusest', {});
    }
};
var components = [engmodeIDOMExtension];
if ('generateNSGetFactory' in XPCOMUtils) {
    this.NSGetFactory = XPCOMUtils.generateNSGetFactory(components);
} else {
    this.NSGetModule = XPCOMUtils.generateNSGetModule(components);
}
"use strict";module.metadata={"stability":"unstable"};const{Cu}=require("chrome");const{ShimWaiver}=Cu.import("resource://gre/modules/ShimWaiver.jsm");
function singularify(text){return text[text.length-1]==="s"?text.substr(0,text.length-1):text;}




function getInitializerName(category){return"init"+singularify(category);}
function on(element,type,listener,capture,shimmed=false){capture=capture||false;if(shimmed){element.addEventListener(type,listener,capture);}else{ShimWaiver.getProperty(element,"addEventListener")(type,listener,capture);}}
exports.on=on;function once(element,type,listener,capture,shimmed=false){on(element,type,function selfRemovableListener(event){removeListener(element,type,selfRemovableListener,capture,shimmed);listener.apply(this,arguments);},capture,shimmed);}
exports.once=once;function removeListener(element,type,listener,capture,shimmed=false){if(shimmed){element.removeEventListener(type,listener,capture);}else{ShimWaiver.getProperty(element,"removeEventListener")(type,listener,capture);}}
exports.removeListener=removeListener;function emit(element,type,{category,initializer,settings},shimmed=false){category=category||"UIEvents";initializer=initializer||getInitializerName(category);let document=element.ownerDocument;let event=document.createEvent(category);event[initializer].apply(event,[type].concat(settings));if(shimmed){element.dispatchEvent(event);}else{ShimWaiver.getProperty(element,"dispatchEvent")(event);}};exports.emit=emit;
const removed=element=>{return new Promise(resolve=>{const{MutationObserver}=element.ownerDocument.defaultView;const observer=new MutationObserver(mutations=>{for(let mutation of mutations){for(let node of mutation.removedNodes||[]){if(node===element){observer.disconnect();resolve(element);}}}});observer.observe(element.parentNode,{childList:true});});};exports.removed=removed;const when=(element,eventName,capture=false,shimmed=false)=>new Promise(resolve=>{const listener=event=>{if(shimmed){element.removeEventListener(eventName,listener,capture);}else{ShimWaiver.getProperty(element,"removeEventListener")(eventName,listener,capture);}
resolve(event);};if(shimmed){element.addEventListener(eventName,listener,capture);}else{ShimWaiver.getProperty(element,"addEventListener")(eventName,listener,capture);}});exports.when=when;
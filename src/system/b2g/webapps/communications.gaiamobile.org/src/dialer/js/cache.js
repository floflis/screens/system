/* (c) 2017 KAI OS TECHNOLOGIES (HONG KONG) LIMITED All rights reserved. This
 * file or any portion thereof may not be reproduced or used in any manner
 * whatsoever without the express written permission of KAI OS TECHNOLOGIES
 * (HONG KONG) LIMITED. KaiOS is the trademark of KAI OS TECHNOLOGIES (HONG KONG)
 * LIMITED or its affiliate company and may be registered in some jurisdictions.
 * All other trademarks are the property of their respective owners.
 */

(function(exports) {
    'use strict';
    var defaultServiceId = -2;
    var pendingSave = null;
    setTimeout(() => {
      var key = 'ril.telephony.defaultServiceId';
      var mozSettings = navigator.mozSettings;
      var req = mozSettings.createLock().get(key);
      req.onsuccess = function() {
        defaultServiceId = req.result[key];
      }
      mozSettings.addObserver(key, function observer(event) {
        defaultServiceId = event.settingValue;
      });
    }, 1500);
    var calllogcache = {
      saveFromNode: function cache_saveFromNode(moduleId, node) {
        // same as defined in call_log.js render
        var MAX_CACHE_CALLLOG_ITEMS = 7;
        var codeNode = this.cloneAsInertNode(node);
        var logItems = codeNode.querySelectorAll('.log-item');
        var i = 0;
        var contactIcon;
        var sections;
        var groupNum = Math.min(logItems.length, MAX_CACHE_CALLLOG_ITEMS);
        var logItem;
        for (i = groupNum; i < logItems.length; i++) {
          let logItem = logItems[i];
          let olContainer = logItem.parentNode;
          let section = olContainer.parentNode;
          olContainer.removeChild(logItem);
          if (!olContainer.children.length) {
            codeNode.removeChild(section);
          }
        }
        var sections = codeNode.querySelectorAll('section');
        for (i = 0; i < sections.length; i++) {
          sections[i].classList.remove('groupFiltered');
        }

        logItems = codeNode.querySelectorAll('.log-item');
        for (i = 0; i < logItems.length; i++) {
          logItems[i].classList.remove('focus');
          logItems[i].classList.remove('filtered');
          logItems[i].removeAttribute('data-nav-id');
          contactIcon = logItems[i].querySelector('.contact-type-icon');
          contactIcon.style.backgroundImage = '';
          contactIcon.classList.add('default');
        }
        if (logItems.length) {
          logItems[0].classList.add('focus');
        }

        var dataL10ns = codeNode.querySelectorAll('[data-l10n-id]');
        var _ = navigator.mozL10n.get;
        for (i = 0; i < dataL10ns.length; i++) {
          if (!dataL10ns[i].textContent) {
            dataL10ns[i].textContent = _(dataL10ns[i].getAttribute('data-l10n-id'));
          }
        }
        var html = codeNode.innerHTML;
        if (pendingSave) {
          clearTimeout(pendingSave);
          pendingSave = null;
        }
        if (defaultServiceId === -2) {
          pendingSave = setTimeout(() => {
            this.save(moduleId, html);
            pendingSave = null;
          }, 2000);
        } else {
          this.save(moduleId, html);
        }
      },

      save: function cache_save(moduleId, html) {
        var id = moduleId;
        var rskContent = navigator.mozL10n.get('softkey-options');
        var cskContent = navigator.mozL10n.get('call');
        var headContent = navigator.mozL10n.get('callLog');

        var lang = navigator.language;
        var tabFilters = [
          '#all-filter > span',
          '#missed-filter > span',
          '#dialed-filter > span',
          '#received-filter > span'
        ];
        var filterTextContent = '';
        var span;
        var _ = navigator.mozL10n.get;
        for (var i = 0; i < tabFilters.length; i++) {
          span = document.querySelector(tabFilters[i]);
          filterTextContent += ',' + _(span.getAttribute('data-l10n-id'));
        }
        html = window.HTML_CACHE_VERSION + ',' + lang +
          ',' + defaultServiceId + ',' + cskContent + ',' + rskContent + ',' +
          headContent + filterTextContent + ':' + html;
        localStorage.setItem('html_cache_' + id, html);
      },

      cloneAsInertNode: function cache_cloneAsInertNode(node) {
        var templateNode = document.createElement('template');
        var cacheDoc = templateNode.content.ownerDocument;
        return cacheDoc.importNode(node, true);
      }
    };

    exports.CallLogCache = calllogcache;
})(this);

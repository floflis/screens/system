'use strict';

/* global Promise */
(function(exports) {
  /**
   * This Voicemail module provides an easy way to identify if any given number
   * is voicemail number or not. Voicemail number could be stored in mozSettings
   * under key of 'ril.iccInfo.mbdn' or in navigator.mozVoicemail. User of this
   * module don't need to know where voicemail number stores, just query
   * voicemail number by invoking Voicemail.check()
   *
   * @example
   * Voicemail.check('123', cardIndex).then(function(isVoicemailNumber) {
   *   // do something based on value of isVoicemailNumber
   * });
   *
   */
  var Voicemail = {
    /**
     * Query if a number is voicemail number or not
     *
     * @param {String} number - Number in query
     * @param {Number} [cardIndex] - SIM card index in query, 0 if not specified
     * @returns {bool} - That retval as true if number is
     *                      voicemail number, false otherwise.
     */
    check: function vm_check(number) {
      if (!number) {
        return false;
      }

      // check the voicemail number if the number is in the sim card
      let voicemail = navigator.mozVoicemail;
      if (voicemail) {
        let voicemailNumber = '';
        for (let i = 0; i < navigator.mozMobileConnections.length; i++) {
          voicemailNumber = voicemail.getNumber(i);
          if (voicemailNumber === number) {
            return true;;
          }
        }
      }

      // check the voicemail number with the mozSetting value
      // based on /shared/resources/apn.json

      if (typeof voicemailNumbers === 'string') {
        return (voicemailNumbers === number);
      } else if (voicemailNumbers) {
        for (let i = 0; i < navigator.mozMobileConnections.length; i++) {
          if (number === voicemailNumbers[i]) {
            return true;
          }
        }
      }
      return false;
    }
  };
  exports.Voicemail = Voicemail;
}(window));

var voicemailNumbers = undefined;
var req = navigator.mozSettings.createLock().get('ril.iccInfo.mbdn');
req.onsuccess = function getVoicemailNumber() {
  voicemailNumbers = req.result['ril.iccInfo.mbdn'];
};

navigator.mozSettings.addObserver('ril.iccInfo.mbdn', (evt) => {
  voicemailNumbers = evt.settingValue;
  if (document.hidden) {
    window.close();
  }
});

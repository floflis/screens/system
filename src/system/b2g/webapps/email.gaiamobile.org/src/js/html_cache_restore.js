var _xstart = performance.timing.fetchStart - performance.timing.navigationStart;

window.plog = function(e) {
    console.log(e + " " + (performance.now() - _xstart));
}, function() {
    function e(e) {
        if (window.mozPerfHasListener) {
            var t = window.performance.now(), n = Date.now();
            setTimeout(function() {
                var o = {
                    name: e,
                    timestamp: t,
                    epoch: n
                }, r = new CustomEvent("x-moz-perf", {
                    detail: o
                });
                window.dispatchEvent(r);
            });
        }
    }
    [ "moz-chrome-dom-loaded", "moz-chrome-interactive", "moz-app-visually-complete", "moz-content-interactive", "moz-app-loaded" ].forEach(function(t) {
        window.addEventListener(t, function() {
            e(t);
        }, !1);
    }), window.PerformanceTestingHelper = {
        dispatch: e
    };
}(), window.startupCacheEventsSent = !1, window.HTML_CACHE_VERSION = '253135cf39e50427cf670820d8dd8fd39f3ecede', window.startupOnModelLoaded = null, 
window.appDispatchedMessage = !1, function() {
    function e(e, t) {
        e.latestOnce("acctsSlice", function() {
            console.log("localOnModelLoaded called, hasAccount: " + localStorage.getItem("data_has_account")), 
            n(), c(h.view), window.startupOnModelLoaded = null, t();
        });
    }
    function t() {
        var e = "yes" === localStorage.getItem("data_has_account");
        return e;
    }
    function n() {
        t() ? h.view || (h.view = "message_list") : h.view = "welcome_page";
    }
    function o(e) {
        if (u && u !== e) return console.log("Ignoring message of type: " + e), !1;
        var t = Date.now();
        if (u) u = null; else if (f + 1e3 > t) return console.log("email entry gate blocked fast repeated action: " + e), 
        !1;
        return f = t, !0;
    }
    function r(e) {
        var t, n, o, r, i = localStorage.getItem("html_cache_" + e) || "";
        if (t = i.indexOf(":"), -1 === t) i = ""; else {
            n = i.substring(0, t), i = i.substring(t + 1);
            var s = n.split(",");
            n = s[0], o = s[1], r = s[2];
        }
        return n !== window.HTML_CACHE_VERSION && (console.log("Skipping html cache for " + e + ", out of date. Expected " + window.HTML_CACHE_VERSION + " but found " + n), 
        i = ""), {
            langDir: o,
            cachedSoftkey: r,
            contents: i
        };
    }
    function i(e, t) {
        return window.appDispatchedMessage = !0, m[e] ? m[e].apply(void 0, t) : (g[e].push(t), 
        d(), void 0);
    }
    function s(e) {
        return console.log("mozSetMessageHandler: received an activity"), o("activity") ? (l && (h.view = "compose", 
        c(h.view)), i("activity", [ e ]), void 0) : e.postError("cancelled");
    }
    function a(e) {
        if (console.log("mozSetMessageHandler: received a notification"), !e.clicked) return "notification" !== h.entry || window.appDispatchedMessage || (console.log("App only started for notification close, closing app."), 
        window.close()), void 0;
        if (o("notification")) {
            "undefined" != typeof Notification && Notification.get && Notification.get().then(function(t) {
                t && t.some(function(t) {
                    return t.tag === e.tag && t.close ? (t.close(), !0) : void 0;
                });
            });
            var t = e.data && e.data.type;
            l && t && (h.view = t, c(t)), setTimeout(function() {
                document.hidden && navigator.mozApps && (console.log("document was hidden, showing app via mozApps.getSelf"), 
                navigator.mozApps.getSelf().onsuccess = function(e) {
                    var t = e.target.result;
                    t.launch();
                });
            }, 300), i("notification", [ e.data ]);
        }
    }
    function c(e) {
        var t = r(e);
        t.langDir && document.querySelector("html").setAttribute("dir", t.langDir);
        var n = t.contents, o = document.getElementById(y.dataset.targetid);
        o.innerHTML = n, window.startupCacheEventsSent = !!n;
        var i = t.cachedSoftkey;
        if (i) {
            var s = new DOMParser().parseFromString(i, "text/html").activeElement.childNodes[0];
            document.body.appendChild(s);
        }
        n && console.log("Using HTML cache for " + e), window.startupCacheEventsSent && (window.performance.mark("navigationLoaded"), 
        window.dispatchEvent(new CustomEvent("moz-chrome-dom-loaded")), window.performance.mark("visuallyLoaded"), 
        window.dispatchEvent(new CustomEvent("moz-app-visually-complete")));
    }
    function d() {
        if (l) {
            var e = document.createElement("script"), t = y.dataset.loader, n = y.dataset.loadsrc;
            t ? (e.setAttribute("data-main", n), e.src = t) : e.src = n, document.head.appendChild(e), 
            l = !1;
        }
    }
    var u = null, l = !0, h = {};
    localStorage.getItem("data_has_account") || (console.log("data_has_account unknown, asking for model load first"), 
    window.startupOnModelLoaded = e), window.startupOnModelLoaded || n(), h.entry = "default";
    var p, f = 0, m = {}, g = {
        notification: [],
        activity: []
    };
    window.globalOnAppMessage = function(e) {
        return Object.keys(e).forEach(function(t) {
            var n = m[t] = e[t], o = g[t];
            o.length && (g[t] = [], o.forEach(function(e) {
                n.apply(void 0, e);
            }));
        }), p || require([ "evt" ], function(e) {
            p = e, p.on("notification", a);
        }), h;
    }, window.globalOnAppMessage.hasAccount = t;
    var y = document.querySelector("[data-loadsrc]");
    if (navigator.mozHasPendingMessage) if (navigator.mozHasPendingMessage("activity") ? u = "activity" : navigator.mozHasPendingMessage("notification") && (u = "notification"), 
    u) h.entry = u; else if (navigator.mozHasPendingMessage("alarm")) h.entry = "alarm"; else if (navigator.mozHasPendingMessage("request-sync")) {
        navigator.mozSetMessageHandler && navigator.mozSetMessageHandler("request-sync", function() {
            console.log("Received legacy request-sync message, ignoring.");
        });
        var v = navigator.sync;
        v && v.registrations().then(function(e) {
            e.forEach(function(e) {
                console.log("Unregistering legacy request sync..."), v.unregister(e.task), console.log("Unregister done!");
            });
        }, function(e) {
            console.error("navigator.sync.registrations failed: ", e);
        });
    }
    "mozSetMessageHandler" in navigator ? (navigator.mozSetMessageHandler("notification", a), 
    navigator.mozSetMessageHandler("activity", s)) : console.warn("mozSetMessageHandler not available. No notifications, activities or syncs."), 
    document.body.classList.toggle("large-text", navigator.largeTextEnabled), window.startupOnModelLoaded ? d() : ("default" === h.entry || "alarm" === h.entry) && (c(h.view), 
    d());
}();
define([], function() {
    return function(e, t, n) {
        return Object.keys(t).forEach(function(o) {
            (!e.hasOwnProperty(o) || n) && (e[o] = t[o]);
        }), e;
    };
});
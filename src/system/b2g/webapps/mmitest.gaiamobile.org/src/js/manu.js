/* © 2017 KAI OS TECHNOLOGIES (HONG KONG) LIMITED All rights reserved.
 * This file or any portion thereof may not be reproduced or used in any manner
 * whatsoever without the express written permission of KAI OS TECHNOLOGIES
 * (HONG KONG) LIMITED. KaiOS is the trademark of KAI OS TECHNOLOGIES (HONG KONG)
 * LIMITED or its affiliate company and may be registered in some jurisdictions.
 * All other trademarks are the property of their respective owners.
 */
// ************************************************************************
// * File Name: manu.js
// * Description: mmitest -> manual test part.
// * Note: When you want to add a new test item, just add the html file
// *       name in manu.html
// ************************************************************************

/* global dump SimpleNavigationHelper*/
'use strict';

function debug(s) {
  if (DEBUG) {
    dump('<mmitest> ------: [manu.js] = ' + s + '\n');
  }
}

var ManuTest = {
  get iframe() {
    return document.getElementById('test-iframe');
  },

  get manuList() {
    return document.getElementById('manuList');
  },

  get manuPanel() {
    return document.getElementById('manu-panel');
  },

  get testPanel() {
    return document.getElementById('test-panel');
  },

  initManuList: function() {
    let container = document.getElementById('manuList');
    let ul = document.createElement('ul');

    function createListItem(name, info) {
      let li = document.createElement('li');
      let p = document.createElement('p');
      p.textContent = info;
      li.setAttribute('data-name', name);
      li.appendChild(p);
      li.classList.add('focusable');
      li.tabIndex = 0;
      ul.appendChild(li);
    };

    getMenuList().then((list) => {
      let i = 0,
        len = list.length;

      for (i; i < len; i++) {
        let item = list[i];
        if (!item.manuTest.isHidden) {
          createListItem(item.htmlId, item.itemName);
        }
      }
    });

    container.appendChild(ul);

    this.navigator = new SimpleNavigationHelper('.focusable', container);
    container.focus();
  },

  startGps: function _startGps() {
    if (navigator.engmodeExtension) {
      navigator.engmodeExtension.startGpsTest();
    }
  },

  stopGps: function _stopGps() {
    if (navigator.engmodeExtension) {
      navigator.engmodeExtension.stopGpsTest();
    }
  },

  openTest: function ut_openTest(name) {
    this.testPanel.classList.remove('hidden');
    this.manuPanel.classList.add('hidden');
    this.iframe.src = name + '.html';
    this.iframe.focus();

    this.currentTest = name;
  },

  closeTest: function ut_closeTest() {
    this.testPanel.classList.add('hidden');
    this.manuPanel.classList.remove('hidden');
    this.iframe.src = '';
    this.manuList.focus();
  },

  init: function ut_init() {
    // Init this manu test list item
    this.initManuList();

    // start gps  to speed up test result.
    this.startGps();

    // Don't let the phone go to sleep while in mmitest.
    // user must manually close it
    if (navigator.requestWakeLock) {
      navigator.requestWakeLock('screen');
    }

    // Init test result list in asycstorage
    ResultManager.getResultList();
  },

  handleEvent: function ut_handleEvent(evt) {
    switch (evt.type) {
      case 'click':
        if (evt.name === 'pass') {
          ResultManager.saveResult(this.currentTest, 'pass');
        } else if (evt.name == 'fail') {
          ResultManager.saveResult(this.currentTest, 'fail');
        }

        this.closeTest();
        break;
      default:
        break;
    }
  },

  handleKeydown: function ut_handleKeydown(evt) {
    evt.preventDefault();
    var target = evt.target;
    switch (evt.key) {
      case 'Backspace':
        this.stopGps();
        window.location = '../index.html';
        break;
      case 'Enter':
        if (target.dataset.name) {
          this.openTest(target.dataset.name);
        }
        break;
      default:
        break;
    }
  }
};

window.onload = ManuTest.init.bind(ManuTest);
window.addEventListener('keydown', ManuTest.handleKeydown.bind(ManuTest));

define(['require','shared/toaster','modules/settings_panel','modules/settings_service','shared/settings_listener'],function(require) {
  
  var Toaster = require('shared/toaster');
  var SettingsPanel = require('modules/settings_panel');
  var SettingsService = require('modules/settings_service');
  var SettingsListener = require('shared/settings_listener');

  return function nfc_settings_panel() {
    var NFC_KEY = 'nfc.enabled';
    var _settings = window.navigator.mozSettings;
    var _currentSettingsValue = false;
    var _nfcSwitchOn,
      _nfcSwitchOff;

    function _initSoftKey() {
      var softkeyParams = {
        menuClassName: 'menu-button',
        header: {
          l10nId: 'message'
        },
        items: [{
          name: 'Cancel',
          l10nId: 'cancel',
          priority: 1,
          method: function() {
            SettingsService.navigate('root');
          }
        }, {
          name: 'Select',
          l10nId: 'select',
          priority: 2,
          method: function() {}
        }]
      };
      SettingsSoftkey.init(softkeyParams);
      SettingsSoftkey.show();
    }

    function _updateNfcInfo(enabled) {
      _currentSettingsValue = enabled;
      _nfcSwitchOn.checked = enabled;
      _nfcSwitchOff.checked = !enabled;
    }

    function _setNfc(evt) {
      var enabled = (evt.target.value === 'true') || false;
      if (_currentSettingsValue === enabled) {
        return;
      }

      var lock = _settings.createLock();
      var option = {};
      option[NFC_KEY] = enabled;
      var req = lock.set(option);

      req.onsuccess = () => {
        var toast = {
          messageL10nId: 'changessaved',
          latency: 2000,
          useTransition: true
        };
        Toaster.showToast(toast);
        SettingsService.navigate('root');
      };

      _nfcSwitchOn.checked = enabled;
      _nfcSwitchOff.checked = !enabled;
    }

    return SettingsPanel({
      onInit: function(panel) {
        _nfcSwitchOn =
          document.getElementById('nfc_switch_on');
        _nfcSwitchOff =
          document.getElementById('nfc_switch_off');
      },

      onBeforeShow: function() {
        _initSoftKey();
        _nfcSwitchOn.addEventListener('click', _setNfc);
        _nfcSwitchOff.addEventListener('click', _setNfc);
        SettingsListener.observe(NFC_KEY, false, _updateNfcInfo);
      },

      onBeforeHide: function() {
        SettingsSoftkey.hide();
        _nfcSwitchOn.removeEventListener('click', _setNfc);
        _nfcSwitchOff.removeEventListener('click', _setNfc);
        SettingsListener.unobserve(NFC_KEY, _updateNfcInfo);
      }
    });
  };
});

/**
 * Created by tclxa on 17-8-24.
 */

(function(exports) {
    'use strict';

    var messagecache = {
      saveFromNode: function cache_saveFromNode(moduleId, node) {
        var html = node.outerHTML;
        this.save(moduleId, html);
      },

      save: function cache_save(moduleId, html) {
        var id = moduleId;

        var langDir = document.querySelector('html').getAttribute('dir');
        var lang = navigator.language;
        var cachedSoftkeyHtml;
        if (!Startup.isActivity) {
          var softkey = document.getElementById('softkeyPanel');
          var cachedSoftkeyNode = this
            .cloneAsInertNodeAvoidingCustomElementHorrors(softkey);
          cachedSoftkeyNode.setAttribute('id', 'cachedSoftkeyPanel');
          cachedSoftkeyHtml = cachedSoftkeyNode.outerHTML;
        } else {
          cachedSoftkeyHtml = MessageCacheRestore.softkeyCacheBackup;
        }

        html = window.HTML_CACHE_VERSION + (langDir ? ',' + langDir : '') +
               (lang ? ',' + lang : '' ) + ',' + cachedSoftkeyHtml + ':' + html;
        localStorage.setItem('html_cache_' + id, html);
      },

      clear: function cache_clear(moduleId) {
        localStorage.removeItem('html_cache_' + moduleId);
      },

      cloneAsInertNodeAvoidingCustomElementHorrors:
        function cache_cloneAsInertNodeAvoidingCustomElementHorrors(node) {
        var templateNode = document.createElement('template');
        var cacheDoc = templateNode.content.ownerDocument;
        return cacheDoc.importNode(node, true);
      }
    };

    exports.MessageCache = messagecache;
})(this);

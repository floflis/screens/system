/*global WeakMap */

/* exported Dialog */

(function(exports) {
'use strict';

/*
 Generic confirm screen. Only 'cancel/default' is mandatory.

 Text fields are localized using commonly defined parameter structure:
 https://developer.mozilla.org/en-US/Firefox_OS/Developing_Gaia/
 localization_code_best_practices#Writing_APIs_that_operate_on_L10nIDs + 'raw'
 can be Node aw well.

 The constructor parameter should follow the following structure:

 {
  title: { raw: 'Non localizable title' },
  body: {
    id: 'localizableStringWithArgument',
    args: { n: count }
  },
  classes: ['specific-class'], // optional
  options: {
    // Cancel is a mandatory option. You need to define at least the text.
    cancel: {
      text: 'localizableCancelLabel'
    },
    confirm: {
      text: 'localizableRemoveLabel',
      method: function(params) {
        fooFunction(params);
      },
      params: [arg1, arg2,....],
      className: 'optionalClassName'
    }
  }
*/

// helper to localize an element given parameters
function createLocalizedElement(tagName, valueL10n) {
  var element = document.createElement(tagName);

  // if we passed an l10nId, use the l10n `setAttributes' method
  if (typeof valueL10n === 'string') {
    element.setAttribute('data-l10n-id', valueL10n);
  } else if (valueL10n.id) {
    navigator.mozL10n.setAttributes(element, valueL10n.id, valueL10n.args);
  // if we passed in a HTML Fragment, it is already localized
  } else if (valueL10n.raw && valueL10n.raw.nodeType) {
    element.appendChild(valueL10n.raw);
  // otherwise - stuff text in here...
  } else {
    element.textContent = valueL10n.raw;
  }
  return element;
}

var dialog = null;

var Dialog = function(params) {
  // Use confirm dialog helper, should refactor later.
  var dialogConfig = {
    title: params.title,
    body: params.body,
    desc: {id: '', args: {}},
    accept: {
      l10nId: 'confirm-dialog-ok',
      icon: 'ok',
      priority: 2,
      callback: function() {
        ThreadUI.confirmDialogShown = false;
        if (params.options && params.options.confirm && params.options.confirm.method) {
          params.options.confirm.method();
        }
      }
    },
    backcallback: function() {
      ThreadUI.confirmDialogShown = false;
    }
  };

  dialog = new ConfirmDialogHelper(dialogConfig);
};

// We prototype functions to show/hide the UI of action-menu
Dialog.prototype.show = function() {
  dialog.show(document.getElementById('file-large-confirmation-dialog'));
};

Dialog.prototype.hide = function() {
  // do nothing
};

exports.Dialog = Dialog;

// end global closure
}(this));


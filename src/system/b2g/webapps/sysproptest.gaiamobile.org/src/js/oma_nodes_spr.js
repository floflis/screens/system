'use strict';

var omaNodes_SPR =(function(){
	var PROPERTY_TYPES = [
		"Roam Properties", 
		"Hidden Menu Properties",
		"Customization ADC",
		"Customization",
		"Subscriber Information",
		"DM Bearer",
		"UICC",
		"Device Info",
		"Others"
	];
	
	var PROPERTY_IDS = [];
	//Roam Properties 24
	PROPERTY_IDS[PROPERTY_TYPES[0]] =[
			{name:constant_SPR.NODE_SYSPROP_DOMDATA_ROAMGUARD, value:constant_SPR.SYSPROP_DOMDATA_ROAMGUARD}, 
			{name:constant_SPR.NODE_SYSPROP_DOMDATA_ROAMGUARD_FORCED, value:constant_SPR.SYSPROP_DOMDATA_ROAMGUARD_FORCED}, 
			{name:constant_SPR.NODE_SYSPROP_BARDOMDATA_ROAM, value:constant_SPR.SYSPROP_BARDOMDATA_ROAM}, 
			{name:constant_SPR.NODE_SYSPROP_DOMDATA_ROAM_FORCED, value:constant_SPR.SYSPROP_DOMDATA_ROAM_FORCED}, 
			{name:constant_SPR.NODE_SYSPROP_DOMVOICE_ROAMGUARD, value:constant_SPR.SYSPROP_DOMVOICE_ROAMGUARD}, 
			{name:constant_SPR.NODE_SYSPROP_DOMVOICE_ROAMGUARD_FORCED, value:constant_SPR.SYSPROP_DOMVOICE_ROAMGUARD_FORCED}, 
			{name:constant_SPR.NODE_SYSPROP_BARDOMVOICE_ROAM, value:constant_SPR.SYSPROP_BARDOMVOICE_ROAM}, 
			{name:constant_SPR.NODE_SYSPROP_DOMVOICE_ROAM_FORCED, value:constant_SPR.SYSPROP_DOMVOICE_ROAM_FORCED}, 
			{name:constant_SPR.NODE_SYSPROP_INTLDATA_ROAMGUARD, value:constant_SPR.SYSPROP_INTLDATA_ROAMGUARD}, 
			{name:constant_SPR.NODE_SYSPROP_INTLDATA_ROAMGUARD_FORCED, value:constant_SPR.SYSPROP_INTLDATA_ROAMGUARD_FORCED}, 
			{name:constant_SPR.NODE_SYSPROP_INTLDATA_ROAM, value:constant_SPR.SYSPROP_INTLDATA_ROAM}, 
			{name:constant_SPR.NODE_SYSPROP_INTLDATA_ROAM_FORCED, value:constant_SPR.SYSPROP_INTLDATA_ROAM_FORCED}, 
			{name:constant_SPR.NODE_SYSPROP_INTLVOICE_ROAMGUARD, value:constant_SPR.SYSPROP_INTLVOICE_ROAMGUARD}, 
			{name:constant_SPR.NODE_SYSPROP_INTLVOICE_ROAMGUARD_FORCED, value:constant_SPR.SYSPROP_INTLVOICE_ROAMGUARD_FORCED}, 
			{name:constant_SPR.NODE_SYSPROP_BARINTLVOICE_ROAM, value:constant_SPR.SYSPROP_BARINTLVOICE_ROAM}, 
			{name:constant_SPR.NODE_SYSPROP_BARINTLVOICE_ROAM_FORCED, value:constant_SPR.SYSPROP_BARINTLVOICE_ROAM_FORCED}, 
			{name:constant_SPR.NODE_SYSPROP_BARLTEDATA_ROAM, value:constant_SPR.SYSPROP_BARLTEDATA_ROAM}, 
			{name:constant_SPR.NODE_SYSPROP_BARLTEDATA_ROAM_FORCED, value:constant_SPR.SYSPROP_BARLTEDATA_ROAM_FORCED}, 
			{name:constant_SPR.NODE_SYSPROP_LTEDATA_ROAM_ENABLED, value:constant_SPR.SYSPROP_LTEDATA_ROAM_ENABLED}, 
			{name:constant_SPR.NODE_SYSPROP_LTEDATA_ROAMGUARD_FORCED, value:constant_SPR.SYSPROP_LTEDATA_ROAMGUARD_FORCED}, 
			{name:constant_SPR.NODE_SYSPROP_BARINTL_LTEDATA_ROAM, value:constant_SPR.SYSPROP_BARINTL_LTEDATA_ROAM}, 
			{name:constant_SPR.NODE_SYSPROP_BARINTL_LTEDATA_ROAM_FORCED, value:constant_SPR.SYSPROP_BARINTL_LTEDATA_ROAM_FORCED}, 
			{name:constant_SPR.NODE_SYSPROP_INTLLTEDATA_ROAMGUARD, value:constant_SPR.SYSPROP_INTLLTEDATA_ROAMGUARD}, 
			{name:constant_SPR.NODE_SYSPROP_INTLLTEDATA_ROAMGUARD_FORCED, value:constant_SPR.SYSPROP_INTLLTEDATA_ROAMGUARD_FORCED}, 
	];
	
	//Hidden Menu Properties 7
	PROPERTY_IDS[PROPERTY_TYPES[1]] =[
			{name:constant_SPR.NODE_OEM_LIFE_TIMER, value:constant_SPR.OEM_LIFE_TIMER},
			{name:constant_SPR.NODE_OEM_LIFE_CALLS, value:constant_SPR.OEM_LIFE_CALLS},
			{name:constant_SPR.NODE_OEM_LIFE_DATA, value:constant_SPR.OEM_LIFE_DATA},
			{name:constant_SPR.NODE_OEM_RECONDITIONED_STATUS, value:constant_SPR.OEM_RECONDITIONED_STATUS},
			{name:constant_SPR.NODE_OEM_RECONDITIONED_DATES, value:constant_SPR.OEM_RECONDITIONED_DATES},
			{name:constant_SPR.NODE_OEM_RECONDITIONED_VERSION, value:constant_SPR.OEM_RECONDITIONED_VERSION},
			{name:constant_SPR.NODE_OEM_ACTIVATION_DATE, value:constant_SPR.OEM_ACTIVATION_DATE},
	];
	
	//Customization ADC 18
	PROPERTY_IDS[PROPERTY_TYPES[2]] =[
			{name:constant_SPR.NODE_SYSPROP_1STADCINFO, value:constant_SPR.SYSPROP_1STADCINFO},
			{name:constant_SPR.NODE_SYSPROP_2NDADCINFO, value:constant_SPR.SYSPROP_2NDADCINFO},
			{name:constant_SPR.NODE_SYSPROP_3RDADCINFO, value:constant_SPR.SYSPROP_3RDADCINFO},
			{name:constant_SPR.NODE_SYSPROP_4THADCINFO, value:constant_SPR.SYSPROP_4THADCINFO},
			{name:constant_SPR.NODE_SYSPROP_5THADCINFO, value:constant_SPR.SYSPROP_5THADCINFO},
			{name:constant_SPR.NODE_SYSPROP_6THADCINFO, value:constant_SPR.SYSPROP_6THADCINFO},
			{name:constant_SPR.NODE_SYSPROP_7THADCINFO, value:constant_SPR.SYSPROP_7THADCINFO},
			{name:constant_SPR.NODE_SYSPROP_8THADCINFO, value:constant_SPR.SYSPROP_8THADCINFO},
			{name:constant_SPR.NODE_SYSPROP_9THADCINFO, value:constant_SPR.SYSPROP_9THADCINFO},
			{name:constant_SPR.NODE_SYSPROP_10THADCINFO, value:constant_SPR.SYSPROP_10THADCINFO},
			{name:constant_SPR.NODE_SYSPROP_11THADCINFO, value:constant_SPR.SYSPROP_11THADCINFO},
			{name:constant_SPR.NODE_SYSPROP_12THADCINFO, value:constant_SPR.SYSPROP_12THADCINFO},
			{name:constant_SPR.NODE_SYSPROP_13THADCINFO, value:constant_SPR.SYSPROP_13THADCINFO},
			{name:constant_SPR.NODE_SYSPROP_14THADCINFO, value:constant_SPR.SYSPROP_14THADCINFO},
			{name:constant_SPR.NODE_SYSPROP_15THADCINFO, value:constant_SPR.SYSPROP_15THADCINFO},
			{name:constant_SPR.NODE_SYSPROP_16THADCINFO, value:constant_SPR.SYSPROP_16THADCINFO},
			{name:constant_SPR.NODE_SYSPROP_17THADCINFO, value:constant_SPR.SYSPROP_17THADCINFO},
			{name:constant_SPR.NODE_SYSPROP_18THADCINFO, value:constant_SPR.SYSPROP_18THADCINFO},
	];
	
	//Customization 22
	PROPERTY_IDS[PROPERTY_TYPES[3]] =[
			{name:constant_SPR.NODE_SYSPROP_OPERATORID, value:constant_SPR.SYSPROP_OPERATORID},
			{name:constant_SPR.NODE_SYSPROP_BROWSER_UAPROF_URL, value:constant_SPR.SYSPROP_BROWSER_UAPROF_URL},
			{name:constant_SPR.NODE_SYSPROP_TETHEREDDATA_ENABLED, value:constant_SPR.SYSPROP_TETHEREDDATA_ENABLED},
			{name:constant_SPR.NODE_SYSPROP_1STCNTCTINFO, value:constant_SPR.SYSPROP_1STCNTCTINFO},
			{name:constant_SPR.NODE_SYSPROP_2NDCNTCTINFO, value:constant_SPR.SYSPROP_2NDCNTCTINFO},
			{name:constant_SPR.NODE_SYSPROP_3RDCNTCTINFO, value:constant_SPR.SYSPROP_3RDCNTCTINFO},
			{name:constant_SPR.NODE_SYSPROP_4THCNTCTINFO, value:constant_SPR.SYSPROP_4THCNTCTINFO},
			{name:constant_SPR.NODE_SYSPROP_5THCNTCTINFO, value:constant_SPR.SYSPROP_5THCNTCTINFO},
			{name:constant_SPR.NODE_SYSPROP_6THCNTCTINFO, value:constant_SPR.SYSPROP_6THCNTCTINFO},
			{name:constant_SPR.NODE_SYSPROP_ROAMMENUDISPLAY, value:constant_SPR.SYSPROP_ROAMMENUDISPLAY},
			{name:constant_SPR.NODE_SYSPROP_ROAMHOMEONLY, value:constant_SPR.SYSPROP_ROAMHOMEONLY},
			{name:constant_SPR.NODE_SYSPROP_DIAGMSLREQ, value:constant_SPR.SYSPROP_DIAGMSLREQ},
			{name:constant_SPR.NODE_SYSPROP_MMSSERVERURL, value:constant_SPR.SYSPROP_MMSSERVERURL},
			{name:constant_SPR.NODE_SYSPROP_MMSPROXY, value:constant_SPR.SYSPROP_MMSPROXY},
			{name:constant_SPR.NODE_SYSPROP_PAYLOAD_CUSTOMID, value:constant_SPR.SYSPROP_PAYLOAD_CUSTOMID},
			{name:constant_SPR.NODE_SYSPROP_SPEEDDIAL_1, value:constant_SPR.SYSPROP_SPEEDDIAL_1},
			{name:constant_SPR.NODE_SYSPROP_ROAMMENU, value:constant_SPR.SYSPROP_ROAMMENU},
			{name:constant_SPR.NODE_SYSPROP_GPSONE_PDEIP, value:constant_SPR.SYSPROP_GPSONE_PDEIP},
			{name:constant_SPR.NODE_SYSPROP_GPSONE_PDEPORT, value:constant_SPR.SYSPROP_GPSONE_PDEPORT},
			{name:constant_SPR.NODE_SYSPROP_MMS_AUTORETRIEVE_ENABLED, value:constant_SPR.SYSPROP_MMS_AUTORETRIEVE_ENABLED},
			{name:constant_SPR.NODE_SYSPROP_MMS_HTTPHEADER, value:constant_SPR.SYSPROP_MMS_HTTPHEADER},
			{name:constant_SPR.NODE_SYSPROP_OMADM_CICM, value:constant_SPR.SYSPROP_OMADM_CICM},
	];

	//Subscriber Information 9
	PROPERTY_IDS[PROPERTY_TYPES[4]] =[
			{name:constant_SPR.NODE_SYSPROP_ACCT_SUBTYPE, value:constant_SPR.SYSPROP_ACCT_SUBTYPE},
			{name:constant_SPR.NODE_SYSPROP_ACCT_TYPE, value:constant_SPR.SYSPROP_ACCT_TYPE},
			{name:constant_SPR.NODE_SYSPROP_BAN, value:constant_SPR.SYSPROP_BAN},
			{name:constant_SPR.NODE_SYSPROP_BILLING_DATE, value:constant_SPR.SYSPROP_BILLING_DATE},
			{name:constant_SPR.NODE_SYSPROP_SELLER_NAME, value:constant_SPR.SYSPROP_SELLER_NAME},
			{name:constant_SPR.NODE_SYSPROP_CSA, value:constant_SPR.SYSPROP_CSA},
			{name:constant_SPR.NODE_SYSPROP_SUB_STATE, value:constant_SPR.SYSPROP_SUB_STATE},
			{name:constant_SPR.NODE_SYSPROP_USAGE_MODE, value:constant_SPR.SYSPROP_USAGE_MODE},
			{name:constant_SPR.NODE_SYSPROP_ACCT_ZIPCODE, value:constant_SPR.SYSPROP_ACCT_ZIPCODE},
	];

	//DM Bearer 18
	PROPERTY_IDS[PROPERTY_TYPES[5]] =[
			{name:constant_SPR.NODE_SYSPROP_EHRPD, value:constant_SPR.SYSPROP_EHRPD},
			{name:constant_SPR.NODE_SYSPROP_BC10, value:constant_SPR.SYSPROP_BC10},
			{name:constant_SPR.NODE_SYSPROP_ENABLEDLTE, value:constant_SPR.SYSPROP_ENABLEDLTE},
			{name:constant_SPR.NODE_SYSPROP_FORCELTE, value:constant_SPR.SYSPROP_FORCELTE},
			{name:constant_SPR.NODE_SYSPROP_B25_ENABLEMENT, value:constant_SPR.SYSPROP_B25_ENABLEMENT},
			{name:constant_SPR.NODE_SYSPROP_B26_ENABLEMENT, value:constant_SPR.SYSPROP_B26_ENABLEMENT},
			{name:constant_SPR.NODE_SYSPROP_B41_ENABLEMENT, value:constant_SPR.SYSPROP_B41_ENABLEMENT},
			{name:constant_SPR.NODE_SYSPROP_BSRTIMER, value:constant_SPR.SYSPROP_BSRTIMER},
			{name:constant_SPR.NODE_SYSPROP_NEXTLTESCAN, value:constant_SPR.SYSPROP_NEXTLTESCAN},
			{name:constant_SPR.NODE_SYSPROP_BSRMAXTIME, value:constant_SPR.SYSPROP_BSRMAXTIME},
			{name:constant_SPR.NODE_SYSPROP_APN0, value:constant_SPR.SYSPROP_APN0},
			{name:constant_SPR.NODE_SYSPROP_APN1, value:constant_SPR.SYSPROP_APN1},
			{name:constant_SPR.NODE_SYSPROP_APN2, value:constant_SPR.SYSPROP_APN2},
			{name:constant_SPR.NODE_SYSPROP_APN3, value:constant_SPR.SYSPROP_APN3},
			{name:constant_SPR.NODE_SYSPROP_APN4, value:constant_SPR.SYSPROP_APN4},
			{name:constant_SPR.NODE_SYSPROP_APN5, value:constant_SPR.SYSPROP_APN5},
			{name:constant_SPR.NODE_SYSPROP_APN6, value:constant_SPR.SYSPROP_APN6},
			{name:constant_SPR.NODE_SYSPROP_APN7, value:constant_SPR.SYSPROP_APN7},	
	];
	
	//UICC 21
	PROPERTY_IDS[PROPERTY_TYPES[6]] =[
			{name:constant_SPR.NODE_SYSPROP_MDN, value:constant_SPR.SYSPROP_MDN},
			{name:constant_SPR.NODE_SYSPROP_NAI, value:constant_SPR.SYSPROP_NAI},
			{name:constant_SPR.NODE_SYSPROP_SLOT1_AUTH_ALGO_AAA, value:constant_SPR.SYSPROP_SLOT1_AUTH_ALGO_AAA},
			{name:constant_SPR.NODE_SYSPROP_MSID, value:constant_SPR.SYSPROP_MSID},
			{name:constant_SPR.NODE_SYSPROP_SLOT1_SPI_AAA, value:constant_SPR.SYSPROP_SLOT1_SPI_AAA},
			{name:constant_SPR.NODE_SYSPROP_PRIMARY_HOME_AGENT, value:constant_SPR.SYSPROP_PRIMARY_HOME_AGENT},
			{name:constant_SPR.NODE_SYSPROP_SECONDARY_HOME_AGENT, value:constant_SPR.SYSPROP_SECONDARY_HOME_AGENT},
			{name:constant_SPR.NODE_SYSPROP_SLOT1_AUTH_ALGO_HA, value:constant_SPR.SYSPROP_SLOT1_AUTH_ALGO_HA},
			{name:constant_SPR.NODE_SYSPROP_SLOT1_SPI_HA, value:constant_SPR.SYSPROP_SLOT1_SPI_HA },
			{name:constant_SPR.NODE_SYSPROP_SLOT1_MOBILE_IP, value:constant_SPR.SYSPROP_SLOT1_MOBILE_IP},
			{name:constant_SPR.NODE_SYSPROP_SLOT1_REVERSE_TUNNEL, value:constant_SPR.SYSPROP_SLOT1_REVERSE_TUNNEL},
			{name:constant_SPR.NODE_SYSPROP_PRL, value:constant_SPR.SYSPROP_PRL},
			{name:constant_SPR.NODE_SYSPROP_ICCID, value:constant_SPR.SYSPROP_ICCID},
			{name:constant_SPR.NODE_SYSPROP_PRLID, value:constant_SPR.SYSPROP_PRLID},
			{name:constant_SPR.NODE_SYSPROP_SO68, value:constant_SPR.SYSPROP_SO68},
			{name:constant_SPR.NODE_SYSPROP_1XADV_COP0, value:constant_SPR.SYSPROP_1XADV_COP0},
			{name:constant_SPR.NODE_SYSPROP_1XADV_COP1TO7, value:constant_SPR.SYSPROP_1XADV_COP1TO7},
			{name:constant_SPR.NODE_SYSPROP_1XADV_ENABLE, value:constant_SPR.SYSPROP_1XADV_ENABLE},
			{name:constant_SPR.NODE_SYSPROP_CSIM_PROV_OBJ, value:constant_SPR.SYSPROP_CSIM_PROV_OBJ},
			{name:constant_SPR.NODE_SYSPROP_CSIM_COVERAGE_OBJ, value:constant_SPR.SYSPROP_CSIM_COVERAGE_OBJ},
			{name:constant_SPR.NODE_SYSPROP_ACCOLC, value:constant_SPR.SYSPROP_ACCOLC},
	];
	
	//Device Info 14
	PROPERTY_IDS[PROPERTY_TYPES[7]] =[
			{name:constant_SPR.NODE_SYSPROP_DEVID, value:constant_SPR.SYSPROP_DEVID},
			{name:constant_SPR.NODE_SYSPROP_MEID, value:constant_SPR.SYSPROP_MEID},
			{name:constant_SPR.NODE_SYSPROP_ESN, value:constant_SPR.SYSPROP_ESN},
			{name:constant_SPR.NODE_SYSPROP_DEVINFO_DM_VER, value:constant_SPR.SYSPROP_DEVINFO_DM_VER},
			{name:constant_SPR.NODE_SYSPROP_DEVINFO_LANG, value:constant_SPR.SYSPROP_DEVINFO_LANG},
			{name:constant_SPR.NODE_SYSPROP_OMADM_SERVER_ID, value:constant_SPR.SYSPROP_OMADM_SERVER_ID},
			{name:constant_SPR.NODE_SYSPROP_DEVICE_VENDOR, value:constant_SPR.SYSPROP_DEVICE_VENDOR},
			{name:constant_SPR.NODE_SYSPROP_DEVICE_MODEL, value:constant_SPR.SYSPROP_DEVICE_MODEL},
			{name:constant_SPR.NODE_SYSPROP_DEVICE_SOFTWARE_VERSION, value:constant_SPR.SYSPROP_DEVICE_SOFTWARE_VERSION},
			{name:constant_SPR.NODE_SYSPROP_DEVDTL_DEV_TYPE, value:constant_SPR.SYSPROP_DEVDTL_DEV_TYPE},
			{name:constant_SPR.NODE_SYSPROP_DEVDTL_FW_VER, value:constant_SPR.SYSPROP_DEVDTL_FW_VER},
			{name:constant_SPR.NODE_SYSPROP_DEVDTL_HW_VER, value:constant_SPR.SYSPROP_DEVDTL_HW_VER},
			{name:constant_SPR.NODE_SYSPROP_DEVINFO_MAN, value:constant_SPR.SYSPROP_DEVINFO_MAN},
			{name:constant_SPR.NODE_SYSPROP_DEVDTL_LARGE_OBJECT, value:constant_SPR.SYSPROP_DEVDTL_LARGE_OBJECT},
	];
	
	//Others 11
	PROPERTY_IDS[PROPERTY_TYPES[8]] =[
			{name:constant_SPR.NODE_SYSPROP_NETWORK_MODE, value:constant_SPR.SYSPROP_NETWORK_MODE},
			{name:constant_SPR.NODE_OEM_HFA_NUMRETRIES, value:constant_SPR.OEM_HFA_NUMRETRIES},
			{name:constant_SPR.NODE_OEM_HFA_RETRYINTERVAL, value:constant_SPR.OEM_HFA_RETRYINTERVAL},
			{name:constant_SPR.NODE_SYSPROP_SMSOIP_ENABLED, value:constant_SPR.SYSPROP_SMSOIP_ENABLED},
			{name:constant_SPR.NODE_SYSPROP_RTSP_PROXY_ADDRESS, value:constant_SPR.SYSPROP_RTSP_PROXY_ADDRESS},
			{name:constant_SPR.NODE_SYSPROP_RTSP_PROXY_PORT, value:constant_SPR.SYSPROP_RTSP_PROXY_PORT},
			{name:constant_SPR.NODE_OEM_CHAMELEON_OBJECT, value:constant_SPR.OEM_CHAMELEON_OBJECT},
			{name:constant_SPR.NODE_SYSPROP_VOWIFI_ENABLED, value:constant_SPR.SYSPROP_VOWIFI_ENABLED},
			{name:constant_SPR.NODE_SYSPROP_VOWIFI_APP_ENABLED, value:constant_SPR.SYSPROP_VOWIFI_APP_ENABLED},
			{name:constant_SPR.NODE_SYSPROP_SIM_LOCK, value:constant_SPR.SYSPROP_SIM_LOCK},
			{name:constant_SPR.NODE_SYSPROP_CNAP_ENABLED, value:constant_SPR.SYSPROP_CNAP_ENABLED},
	];

	

	function getPropertyTypes() {
		return PROPERTY_TYPES;
	};

	function getPropertyIds(type) {
		return PROPERTY_IDS[type];
	};
	
	return {
		getPropertyTypes: getPropertyTypes,
		getPropertyIds: getPropertyIds
	};
})();


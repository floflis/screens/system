(function(exports) {
    'use strict';
    var DEBUG = false;

    if(!exports.GLOBAL_TASKMGR_ACTION) {
      exports.addEventListener('keydown', function(e) {
        let mgr = taskManager;
        switch(e.key) {
          case 'ArrowLeft':
            mgr.position--;
            if(mgr.currentCard && mgr.currentCard.app) {
              mgr.alignCurrentCard();
            }
            else mgr.position++;
            break;
          case 'ArrowRight':
            mgr.position++;
            if(mgr.currentCard && mgr.currentCard.app) {
              mgr.alignCurrentCard();
            }
            else mgr.position--;
            break;
          case 'ArrowUp':
            if(mgr.currentCard && mgr.currentCard.app) {
              mgr.currentCard.killApp();
            }
            break;
//          case 'ArrowDown':
//            mgr._handle_home();
//            break;
          case 'Enter':
          case 'Backspace':
            if(mgr.currentCard && mgr.currentCard.app) {
              e.preventDefault();
              mgr.exitToApp(mgr.currentCard.app);
            }
            break;
        }
      })
      exports.GLOBAL_TASKMGR_ACTION = 1
    }

    function TaskManager() {
        this.stack = null;
        this.cardsByAppID = {};
    }
    TaskManager.prototype = Object.create({
        SCREENSHOT_PREVIEWS_SETTING_KEY: 'app.cards_view.screenshots.enabled',
        DURATION: 200,
        SWIPE_UP_THRESHOLD: 100,
        SWITCH_CARD_THRESHOLD: 30,
        useAppScreenshotPreviews: true,
        position: 0,
        _shouldGoBackHome: false,
        _active: false,
        windowWidth: window.innerWidth,
        windowHeight: window.innerHeight
    }, {
        currentCard: {
            get: function cs_getCurrentCard() {
                return this.getCardAtIndex(this.position);
            }
        }
    });
    TaskManager.prototype.EVENT_PREFIX = 'taskmanager';
    TaskManager.prototype.name = 'TaskManager';
    TaskManager.prototype.start = function() {
        this._fetchElements();
        this._registerEvents();
        this._appClosedHandler = this._appClosed.bind(this);
        Service.request('registerHierarchy', this);
    };
    TaskManager.prototype.stop = function() {
        this._unregisterEvents();
        Service.request('unregisterHierarchy', this);
    };
    TaskManager.prototype._fetchElements = function() {
        this.element = document.getElementById('cards-view'), this.cardsList = document.getElementById('cards-list');
        this.screenElement = document.getElementById('screen');
    };
    TaskManager.prototype._registerEvents = function() {
        window.addEventListener('taskmanagershow', this);
        this.onPreviewSettingsChange = function(settingValue) {
            this.useAppScreenshotPreviews = settingValue;
        }.bind(this);
        SettingsListener.observe(this.SCREENSHOT_PREVIEWS_SETTING_KEY, this.useAppScreenshotPreviews, this.onPreviewSettingsChange);
    };
    TaskManager.prototype._unregisterEvents = function() {
        window.removeEventListener('taskmanagershow', this);
        SettingsListener.unobserve(this.SCREENSHOT_PREVIEWS_SETTING_KEY, this.onPreviewSettingsChange);
    };
    TaskManager.prototype._appClosed = function cs_appClosed(evt) {
        window.removeEventListener('appclosed', this._appClosedHandler);
        window.removeEventListener('homescreenclosed', this._appClosedHandler);
        this.screenElement.classList.add('cards-view');
        this.element.classList.remove('from-home');
    };
    TaskManager.prototype.show = function cs_showCardSwitcher(filterName) {
        if (this.isShown()) {
            return;
        }
        if (document.mozFullScreen) {
            document.mozCancelFullScreen();
        }
        this.calculateDimensions();
        this.newStackPosition = null;
        this._registerShowingEvents();
        if (this.filter(filterName)) {
            this.element.classList.add('filtered');
        }
        var stack = this.stack;
        stack.forEach(function(app, position) {
            this.addCard(position, app);
        }, this);
        this.unfilteredStack.forEach(function(app, position) {
            app.enterTaskManager();
        });
        this._placeCards();
        this.setActive(true);
        var screenElement = this.screenElement;
        var activeApp = Service.currentApp;
        if (!activeApp) {
            screenElement.classList.add('cards-view');
            return;
        }
        if (activeApp.isHomescreen) {
            activeApp.close('home-to-cardview');
            this.element.classList.add('from-home');
            window.addEventListener('homescreenclosed', this._appClosedHandler);
        } else {
            window.addEventListener('appclosed', this._appClosedHandler);
        }
    };
    TaskManager.prototype.hide = function cs_hideCardSwitcher() {
        if (!this.isActive()) {
            return;
        }
        this._unregisterShowingEvents();
        this._removeCards();
        this.setActive(false);
        window.removeEventListener('appclosed', this._appClosedHandler);
        window.removeEventListener('homescreenclosed', this._appClosedHandler);
        this.screenElement.classList.remove('cards-view');
        var detail;
        if (!isNaN(this.newStackPosition)) {
            detail = {
                'detail': {
                    'newStackPosition': this.newStackPosition
                }
            };
        }
        this.publishNextTick('cardviewclosed', detail);
    };
    TaskManager.prototype._showingEventsRegistered = false;
    TaskManager.prototype._registerShowingEvents = function() {
        if (this._showingEventsRegistered) {
            return;
        }
        this._showingEventsRegistered = true;
        window.addEventListener('lockscreen-appopened', this);
        window.addEventListener('attentionopened', this);
        window.addEventListener('appopen', this);
        window.addEventListener('appterminated', this);
        window.addEventListener('wheel', this);
        window.addEventListener('resize', this);
        this.element.addEventListener('touchstart', this);
        this.element.addEventListener('touchmove', this);
        this.element.addEventListener('touchend', this);
    };
    TaskManager.prototype._unregisterShowingEvents = function() {
        if (!this._showingEventsRegistered) {
            return;
        }
        window.removeEventListener('lockscreen-appopened', this);
        window.removeEventListener('attentionopened', this);
        window.removeEventListener('appopen', this);
        window.removeEventListener('appterminated', this);
        window.removeEventListener('wheel', this);
        window.removeEventListener('resize', this);
        if (this.element) {
            this.element.removeEventListener('touchstart', this);
            this.element.removeEventListener('touchmove', this);
            this.element.removeEventListener('touchend', this);
        }
        this._showingEventsRegistered = false;
    };
    TaskManager.prototype.isShown = function() {
        return this.isActive();
    };
    TaskManager.prototype.isActive = function() {
        return this._active;
    };
    TaskManager.prototype.setActive = function(active) {
        if (active == this._active) {
            return;
        }
        this._active = active;
        if (active) {
            this.publish(this.EVENT_PREFIX + '-activated');
        } else {
            this.publish(this.EVENT_PREFIX + '-deactivated');
        }
        this.element.classList.toggle('active', active);
        this.element.classList.toggle('empty', !this.stack.length && active);
        this.publishNextTick(active ? 'cardviewshown' : 'cardviewbeforeclose');
    };
    TaskManager.prototype.filter = function cs_filterCardStack(filterName) {
        var unfilteredStack = this.unfilteredStack = StackManager.snapshot();
        var noRecentWindows = document.getElementById('cards-no-recent-windows');
        switch (filterName) {
            case 'browser-only':
                this.stack = unfilteredStack.filter(function(app) {
                    return app.isBrowser() || (app.manifest && app.manifest.role === 'search');
                });
                navigator.mozL10n.setAttributes(noRecentWindows, 'no-recent-browser-windows');
                break;
            case 'apps-only':
                this.stack = unfilteredStack.filter(function(app) {
                    return !app.isBrowser();
                });
                navigator.mozL10n.setAttributes(noRecentWindows, 'no-recent-app-windows');
                break;
            default:
                this.stack = unfilteredStack;
                break;
        }
        this.position = this.stack.indexOf(unfilteredStack[StackManager.position]);
        if (this.position === -1 || StackManager.outOfStack()) {
            this.position = this.stack.length - 1;
            this._shouldGoBackHome = true;
        } else {
            this._shouldGoBackHome = false;
        }
        return this.stack !== unfilteredStack;
    };
    TaskManager.prototype.addCard = function cs_addCard(position, app) {
        var config = {
            manager: this,
            position: position,
            app: app,
            windowWidth: this.windowWidth,
            windowHeight: this.windowHeight
        };
        var card = new Card(config);
        this.cardsByAppID[app.instanceID] = card;
        this.cardsList.appendChild(card.render());
        if (position <= this.position - 2 || position >= this.position + 2) {
            card.element.style.visibility = 'hidden';
        }
    };
    TaskManager.prototype.removeCard = function cs_removeCard(card) {
        var element = card.element;
        var position = element.dataset.position;
        delete this.cardsByAppID[card.app.instanceID];
        card.destroy();
        element = null;
        this.stack.splice(position, 1);
        var cardNodes = this.cardsList.childNodes;
        for (var i = position, remainingCard = null; i < cardNodes.length; i++) {
            remainingCard = this.getCardForElement(cardNodes[i]);
            if (remainingCard) {
                remainingCard.position = i;
                cardNodes[i].dataset.position = i;
            }
        }
        var cardsLength = cardNodes.length;
        if (!cardsLength) {
            var homescreen = homescreenLauncher.getHomescreen(true);
            this.exitToApp(homescreen);
        }
        if (cardsLength === this.position) {
            this.position--;
        }
        this.alignCurrentCard();
    };
    TaskManager.prototype._removeCards = function cs_removeCards() {
        this.stack.forEach(function(app, idx) {
            var card = this.cardsByAppID[app.instanceID];
            card && card.destroy();
        }, this);
        this.unfilteredStack.forEach(function(app, position) {
            app.leaveTaskManager();
        });
        this.cardsByAppID = {};
        this.element.classList.remove('filtered');
        this.cardsList.innerHTML = '';
    };
    TaskManager.prototype.cardAction = function cs_cardAction(card, actionName) {
        switch (actionName) {
            case 'close':
                card.killApp();
                break;
            case 'favorite':
                debug('cardAction: TODO: favorite ' + card.element.dataset.origin);
                break;
            case 'select':
                if (this.position != card.position) {
                    this.position = card.position;
                    this.alignCurrentCard();
                }
                var self = this;
                this.currentCard.element.addEventListener('transitionend', function afterTransition(e) {
                    e.target.removeEventListener('transitionend', afterTransition);
                    self.exitToApp(card.app);
                });
                this.currentCard.element.classList.add('select');
                break;
        }
    };
    TaskManager.prototype.exitToApp = function(app) {
        this.screenElement.classList.remove('cards-view');
        this._unregisterShowingEvents();
        if (this._shouldGoBackHome) {
            app = app || homescreenLauncher.getHomescreen(true);
        } else if (!app) {
            app = this.stack ? this.stack[this.position] : homescreenLauncher.getHomescreen(true);
        }
        var position = this.unfilteredStack ? this.unfilteredStack.indexOf(app) : -1;
        if (position !== StackManager.position) {
            this.newStackPosition = position;
        }
        setTimeout(() => {
            var finish = () => {
                this.element.classList.remove('to-home');
                this.hide();
            };
            eventSafety(app.element, '_opened', finish, 400);
            if (app.isHomescreen) {
                this.element.classList.add('to-home');
                app.open('home-from-cardview');
            } else {
                app.open('from-cardview');
            }
        }, 100);
    };
    TaskManager.prototype.handleWheel = function cs_handleWheel(evt) {
        if (evt.deltaMode !== evt.DOM_DELTA_PAGE || evt.deltaY < 0) {
            return;
        }
        if (evt.deltaY > 0) {
            var card = this.currentCard;
            if (card.app.killable()) {
                card.killApp();
            } else {
                card.applyStyle({
                    MozTransform: ''
                });
            }
        } else if (evt.deltaX > 0 && this.position < this.cardsList.childNodes.length - 1) {
            this.position++;
        } else if (evt.deltaX < 0 && this.position > 0) {
            this.position--;
        }
        this.alignCurrentCard();
    };
    TaskManager.prototype.respondToHierarchyEvent = function(evt) {
        if (this['_handle_' + evt.type]) {
            return this['_handle_' + evt.type](evt);
        }
        return true;
    };
    TaskManager.prototype._handle_home = function() {
        if (this.isActive()) {
            this._shouldGoBackHome = true;
            this.exitToApp();
            return false;
        }
        return true;
    };
    TaskManager.prototype._handle_holdhome = function(evt) {
        if (this.isShown()) {
            return true;
        }
        var filter = null;
        if (evt.type === 'taskmanagershow') {
            filter = (evt.detail && evt.detail.filter) || null;
        }
        var currOrientation = OrientationManager.fetchCurrentOrientation();
        var shouldResize = (OrientationManager.defaultOrientation.split('-')[0] != currOrientation.split('-')[0]);
        var shouldHideKeyboard = layoutManager.keyboardEnabled;
        this.publish('cardviewbeforeshow');
        var finish = () => {
            if (shouldHideKeyboard) {
                window.addEventListener('keyboardhidden', function kbHidden() {
                    window.removeEventListener('keyboardhidden', kbHidden);
                    shouldHideKeyboard = false;
                    setTimeout(finish);
                });
                return;
            }
            screen.mozLockOrientation(OrientationManager.defaultOrientation);
            if (shouldResize) {
                window.addEventListener('resize', function resized() {
                    window.removeEventListener('resize', resized);
                    shouldResize = false;
                    setTimeout(finish);
                });
                return;
            }
            var app = Service.currentApp;
            if (app && !app.isHomescreen) {
                app.getScreenshot(function onGettingRealtimeScreenshot() {
                    this.show(filter);
                }.bind(this), 0, 0, 300);
            } else {
                this.show(filter);
            }
        };
        finish();
    };
    TaskManager.prototype.handleTap = function cs_handleTap(evt) {
        var targetNode = evt.target;
        var cardElem = null;
        var tmpNode = targetNode;
        while (tmpNode) {
            if (tmpNode.classList && tmpNode.classList.contains('card')) {
                cardElem = tmpNode;
                break;
            }
            tmpNode = tmpNode.parentNode;
        }
        var card = this.getCardForElement(cardElem);
        if (!card) {
            return;
        }
        if ('buttonAction' in targetNode.dataset) {
            this.cardAction(card, targetNode.dataset.buttonAction);
            return;
        }
        if (('position' in targetNode.dataset) || card) {
            this.cardAction(card, 'select');
            return;
        }
    };
    TaskManager.prototype.calculateDimensions = function cv_calculateDimensions(evt) {
        this.windowWidth = window.innerWidth;
        this.windowHeight = window.innerHeight;
    };
    TaskManager.prototype.handleEvent = function cv_handleEvent(evt) {
        var app;
        switch (evt.type) {
            case 'touchstart':
                this.onTouchStart(evt);
                evt.preventDefault();
                evt.stopPropagation();
                break;
            case 'touchmove':
                this.onTouchMove(evt);
                evt.stopPropagation();
                evt.preventDefault();
                break;
            case 'touchend':
                this.onTouchEnd(evt);
                evt.stopPropagation();
                evt.preventDefault();
                break;
            case 'resize':
                this.calculateDimensions();
                this.alignCurrentCard();
                break;
            case 'wheel':
                this.handleWheel(evt);
                break;
            case 'lockscreen-appopened':
            case 'attentionopened':
                this.exitToApp();
                break;
            case 'taskmanagershow':
                this._handle_holdhome(evt);
                break;
            case 'taskmanagerhide':
            case 'appopen':
                this.hide();
                break;
            case 'appterminated':
                app = evt.detail;
                var card = app && this.cardsByAppID[app.instanceID];
                if (card && card.app && app.instanceID === card.app.instanceID) {
                    this.removeCard(card);
                }
                break;
        }
    };
    TaskManager.prototype.publish = function tm_publish(type, detail) {
        var event = new CustomEvent(type, detail || null);
        window.dispatchEvent(event);
    };
    TaskManager.prototype.publishNextTick = function tm_publish(type, detail) {
        var event = new CustomEvent(type, detail || null);
        setTimeout(function nextTick() {
            window.dispatchEvent(event);
        });
    };
    TaskManager.prototype.getCardAtIndex = function(idx) {
        if (this.stack && idx > -1 && idx < this.stack.length) {
            var app = this.stack[idx];
            var card = app && this.cardsByAppID[app.instanceID];
            if (card) {
                return card;
            }
        }
        debug('getCardAtIndex, no card at idx: ' + idx);
        return null;
    };
    TaskManager.prototype.getCardForElement = function(element) {
        return element && this.cardsByAppID[element.dataset.appInstanceId];
    };
    TaskManager.prototype._setAccessibilityAttributes = function() {
        this.stack.forEach(function(app, idx) {
            var card = this.cardsByAppID[app.instanceID];
            if (!card) {
                return;
            }
            card.setVisibleForScreenReader(idx === this.position);
            card.element.setAttribute('aria-setsize', this.stack.length);
            card.element.setAttribute('aria-posinset', idx + 1);
        }, this);
    };
    TaskManager.prototype._placeCards = function() {
        this.stack.forEach(function(app, idx) {
            var card = this.cardsByAppID[app.instanceID];
            if (!card) {
                return;
            }
            card.move(0, 0);
            card.element.classList.toggle('current', (idx == this.position));
        }.bind(this));
        this._setAccessibilityAttributes();
    };
    TaskManager.prototype.alignCurrentCard = function(duration, callback) {
        this._setupCardsTransition(duration || this.DURATION);
        this._placeCards();
    };
    TaskManager.prototype.moveCards = function() {
        var deltaX = this.deltaX;
        var sign = (deltaX > 0) ? -1 : 1;
        if (this.onExtremity()) {
            deltaX /= 1.5;
        }
        var current = this.position;
        this.stack.forEach(function(app, idx) {
            var card = this.cardsByAppID[app.instanceID];
            if (idx >= current - 2 && idx <= current + 2) {
                card.move(Math.abs(deltaX) * sign);
            }
        }, this);
    };
    TaskManager.prototype.onExtremity = function() {
        var sign = (this.deltaX > 0) ? -1 : 1;
        return (this.position === 0 && sign === 1 || this.position === this.stack.length - 1 && sign === -1);
    };
    TaskManager.prototype.onTouchMoveForDeleting = function(evt) {
        var dx = this.deltaX;
        var dy = this.deltaY;
        if (dy > 0) {
            var card = this.getCardForElement(evt.target);
            if (!card) {
                return;
            }
            if ('function' == typeof card.move) {
                card.move(dx, -dy);
            } else {
                card.applyStyle({
                    transform: 'translateY(' + (-dy) + 'px)'
                });
            }
        }
    };
    TaskManager.prototype.onTouchStart = function cs_onTouchStart(evt) {
        if (this.element.classList.contains('empty')) {
            var homescreen = homescreenLauncher.getHomescreen(true);
            this.exitToApp(homescreen);
            return;
        }
        this._dragPhase = '';
        this.deltaX = 0;
        this.deltaY = 0;
        this.startTouchPosition = [evt.touches[0].pageX, evt.touches[0].pageY];
        this.startTouchDate = Date.now();
        this._resetCardsTransition();
    };
    TaskManager.prototype.onTouchEnd = function cs_onTouchEnd(evt) {
        this.deltaX = evt.changedTouches[0].pageX - this.startTouchPosition[0];
        this.deltaY = evt.changedTouches[0].pageY - this.startTouchPosition[1];
        if (this._dragPhase == 'cross-slide') {
            var element = evt.target;
            var card = this.getCardForElement(element);
            if (!card) {
                return;
            }
            if (-this.deltaY > this.SWIPE_UP_THRESHOLD && card.app.killable()) {
                card.killApp();
            } else {
                card.applyStyle({
                    transform: ''
                });
            }
            this.alignCurrentCard();
            return;
        }
        if (Math.abs(this.deltaX) <= 1) {
            this.handleTap(evt);
            return;
        }
        var speed = this.deltaX / (Date.now() - this.startTouchDate);
        var inertia = speed * 250;
        var boosted = this.deltaX + inertia;
        var progress = Math.abs(boosted) / this.windowWidth;
        if (progress > 0.5) {
            progress -= 0.5;
        }
        var switching = Math.abs(boosted) >= this.SWITCH_CARD_THRESHOLD;
        if (switching) {
            if (this.deltaX < 0 && this.position < this.cardsList.childNodes.length - 1) {
                this.position++;
            } else if (this.deltaX > 0 && this.position > 0) {
                this.position--;
            }
        }
        var durationLeft = Math.max(50, (1 - progress) * this.DURATION);
        this.alignCurrentCard(durationLeft);
    };
    TaskManager.prototype.onTouchMove = function cs_onTouchMove(evt) {
        this.deltaX = this.startTouchPosition[0] - evt.touches[0].pageX;
        this.deltaY = this.startTouchPosition[1] - evt.touches[0].pageY;
        switch (this._dragPhase) {
            case '':
                if (this.deltaY > this.SWIPE_UP_THRESHOLD && evt.target.classList.contains('card')) {
                    this._dragPhase = 'cross-slide';
                    this.onTouchMoveForDeleting(evt);
                } else {
                    if (Math.abs(this.deltaX) > this.SWITCH_CARD_THRESHOLD) {
                        this._dragPhase = 'scrolling';
                    }
                    this.moveCards();
                }
                break;
            case 'cross-slide':
                this.onTouchMoveForDeleting(evt);
                break;
            case 'scrolling':
                this.moveCards();
                break;
        }
    };
    TaskManager.prototype._setupCardsTransition = function(duration) {
        var position = this.position;
        var self = this;
        this.stack.forEach(function(app, idx) {
            var card = self.cardsByAppID[app.instanceID];
            if (idx < position - 2 || idx > position + 2) {
                card.element.style.visibility = 'hidden';
                return;
            }
            card.element.style.visibility = '';
            var style = {
                transition: 'transform ' + duration + 'ms linear'
            };
            card.applyStyle(style);
        });
    };
    TaskManager.prototype._resetCardsTransition = function() {
        var zeroTransitionStyle = {
            transition: ''
        };
        this.stack.forEach(function(app, idx) {
            var card = this.cardsByAppID[app.instanceID];
            card.applyStyle(zeroTransitionStyle);
        }, this);
    };
    exports.TaskManager = TaskManager;

    function debug(message) {
        if (DEBUG) {
            console.log('TaskManager > \n  ', message);
        }
    }
})(window);

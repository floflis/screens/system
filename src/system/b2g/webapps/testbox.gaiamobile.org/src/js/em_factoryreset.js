/**
 * Created by gaoguang on 9/7/16.
 */

'use strict';
$('menuItem-resetphone').addEventListener('click', function() {
  factoryReset.init();
});


var factoryReset = {
  init:function _init(){
    _self = this;
    $('btn_reset').addEventListener('click', _self.factoryReset);
  },

  factoryReset: function _factoryReset() {
    var power = navigator.mozPower;
    if (!power) {
      console.error('Cannot get mozPower');
      return;
    }

    if (!power.factoryReset) {
      console.error('Cannot invoke mozPower.factoryReset()');
      return;
    }

    power.factoryReset();
  }
};





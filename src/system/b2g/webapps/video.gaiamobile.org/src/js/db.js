
function initDB(){videodb=new MediaDB('videos',null);videodb.onupgrading=function(evt){storageState=MediaDB.UPGRADING;updateDialog();};videodb.onunavailable=function(event){storageState=event.detail;if(playerShowing){hidePlayer(true);}
updateDialog();};videodb.oncardremoved=function(){if(playerShowing)
hidePlayer(true);};videodb.onenumerable=function(){storageState=false;enumerateDB();addListener();};videodb.onscanend=function(){if(!firstScanEnded){firstScanEnded=true;window.performance.mark('fullyLoaded');}
if(storageState===MediaDB.UPGRADING){showOverlay('upgrade');}else if(storageState===MediaDB.NOCARD){showOverlay('nocard');}else if(storageState===MediaDB.UNMOUNTED){showOverlay('pluggedin');}else if(firstScanEnded&&thumbnailList.count===0&&metadataQueue.length===0){NavigationMap.setSoftKeyBar(LAYOUT_MODE.list);showOverlay('empty');}};videodb.oncreated=function(event){event.detail.forEach(videoCreated);};videodb.ondeleted=function(event){var count=event.detail.reduce(function(count,filename){var index=deletedItems.indexOf(filename);if(index!=-1){deletedItems.splice(index,1);count++;}
return count;},0);if(count>0){Toaster.showToast({messageL10nId:'n-video-deleted',messageL10nArgs:{n:totalDeletedCount},latency:2000});}
event.detail.forEach(videoDeleted);};}
function addListener(){window.addEventListener('update-enumerate',(evt)=>{const curIndex=evt.detail.index;if(curIndex===this.lastIndex||enumerateDone)return;this.lastIndex=curIndex;this.enumerateDB();});}
var lastIndexDb=0;var lastIndex=null;var enumerateDone=false;function enumerateDB(){var firstBatchDisplayed=false;var batch=[];const limitByLoad=lastIndexDb+10;const enumerateEntry=videoinfo=>{if(videoinfo===null){flush();batch=[];enumerateDone=enumerate.state==='complete';return;}
var isVideo=videoinfo.metadata.isVideo;if(isVideo===false){return;}
if(isVideo===undefined){addToMetadataQueue(videoinfo);return;}
if(isVideo===true){lastIndexDb++;batch.push(videoinfo);}
if(lastIndexDb===limitByLoad){videodb.cancelEnumeration(enumerate);flush();batch=[];return;}};var enumerate=this.lastIndexDb===0?videodb.enumerate('date',null,'prev',enumerateEntry):videodb.advancedEnumerate('date',null,'prev',this.lastIndexDb,enumerateEntry);function flush(){batch.forEach(addVideo);batch.length=0;if(!firstBatchDisplayed){firstBatchDisplayed=true;window.dispatchEvent(new CustomEvent('visual-loaded',{detail:{index:this.lastIndex}}));window.performance.mark('visuallyLoaded');window.performance.mark('contentInteractive');}}}
function addVideo(videodata){if(!videodata||!videodata.metadata.isVideo){return;}
var view=thumbnailList.addItem(videodata,true);view.addTapListener(thumbnailClickHandler);if(thumbnailList.count===1&&firstScanEnded){updateDialog();}}
function videoCreated(videoinfo){addToMetadataQueue(videoinfo);}
function videoDeleted(filename){if(currentVideo&&filename===currentVideo.name){resetCurrentVideo();}
thumbnailList.removeItem(filename);if(thumbnailList.count===0){updateDialog();hideSelectView();}}
initDB();
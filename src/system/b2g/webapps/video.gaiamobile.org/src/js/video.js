'use strict';const LAYOUT_MODE={list:'layout-list',selection:'layout-selection',fullscreenPlayer:'layout-fullscreen-player'};const SUB_MODE={overlay:'overlay',infoView:'info-view',renameView:'rename-view',pickActivity:'pick-activity'};var dom={};var ids=['thumbnails','thumbnails-select-top','thumbnails-number-selected','player-view','thumbnails-single-delete-button','thumbnails-single-share-button','thumbnails-single-info-button','info-view','player','overlay','overlay-title','overlay-text','video-container','videoBar','close','play','playHead','timeSlider','elapsedTime','duration-text','elapsed-text','bufferedTime','slider-wrapper','picker-close','options-view','options-cancel-button','in-use-overlay','in-use-overlay-title','in-use-overlay-text','no-video','file-info','file-rename','thumbnail-views','renameId'];ids.forEach(function createElementRef(name){dom[toCamelCase(name)]=document.getElementById(name);});dom.player.mozAudioChannelType='content';if(navigator.mozAudioChannelManager){navigator.mozAudioChannelManager.volumeControlChannel='content';}
function $(id){return document.getElementById(id);}
var playing=false;var playerShowing=false;var endedTimer;var controlShowing=false;var controlFadeTimeout=null;var selectedFileNames=[];var selectedFileNamesToBlobs={};var videodb;var currentVideo;var currentVideoBlob;var firstScanEnded=false;var THUMBNAIL_WIDTH;var THUMBNAIL_HEIGHT;var HAVE_NOTHING=0;var storageState;var currentOverlay;var dragging=false;var touchStartID=null;var isPausedWhileDragging;var sliderRect;var thumbnailList;var pendingPick;var videoHardwareReleased=false;var restoreTime=null;var isPhone=true;var isPortrait;var currentLayoutMode;var isFullScreen=false;var isFullScreenAction=false;var canPlay=true;var pendingUpdateTitleText=false;var FROMCAMERA=/DCIM\/\d{3}KAIOS\/VID_\d{4}\.3gp$/;var loadingChecker=new VideoLoadingChecker(dom.player,dom.inUseOverlay,dom.inUseOverlayTitle,dom.inUseOverlayText);var deletedItems=[];var totalDeletedCount=0;window.performance.mark('navigationLoaded');window.performance.mark('navigationInteractive');document.addEventListener('visibilitychange',function visibilityChange(){if(document.hidden){stopParsingMetadata();}
else{if(playerShowing){if(videoHardwareReleased){restoreVideo();}
setControlsVisibility(true);window.option.show();VideoUtils.fitContainer(dom.videoContainer,dom.player,currentVideo.metadata.rotation||0);}else{startParsingMetadata();}}});navigator.mozL10n.once(function(){init();});initLayout();initThumbnailSize();function init(){thumbnailList=new ThumbnailList(ThumbnailDateGroup,dom.thumbnails);ThumbnailDateGroup.Template=new Template('thumbnail-group-header');ThumbnailItem.Template=new Template('thumbnail-template');ThumbnailItem.titleMaxLines=isPhone?2:(isPortrait?4:2);if(!navigator.mozHasPendingMessage('activity')){initOptionsButtons();}
initPlayerControls();var acm=navigator.mozAudioChannelManager;if(acm){acm.addEventListener('headphoneschange',function onheadphoneschange(){if(!acm.headphones&&playing){setVideoPlaying(false);}});}
navigator.mozSetMessageHandler('activity',handleActivityEvents);}
function initThumbnailSize(){if(isPhone){THUMBNAIL_WIDTH=240*window.devicePixelRatio;THUMBNAIL_HEIGHT=240*window.devicePixelRatio;}else{var shortEdge=Math.min(window.innerWidth,window.innerHeight);THUMBNAIL_WIDTH=424*window.devicePixelRatio*shortEdge/800;THUMBNAIL_HEIGHT=Math.round(THUMBNAIL_WIDTH*4/7);}}
function initLayout(){ScreenLayout.watch('portrait','(orientation: portrait)');isPortrait=ScreenLayout.getCurrentLayout('portrait');if(isPhone||isPortrait){setDisabled(dom.playerView,false);}else{setDisabled(dom.playerView,true);}
window.addEventListener('screenlayoutchange',handleScreenLayoutChange);switchLayout(LAYOUT_MODE.list);}
function initPlayerControls(){dom.sliderWrapper.addEventListener('touchstart',handleSliderTouchStart);dom.sliderWrapper.addEventListener('touchmove',handleSliderTouchMove);dom.sliderWrapper.addEventListener('touchend',handleSliderTouchEnd);dom.player.addEventListener('timeupdate',timeUpdated);dom.player.addEventListener('seeked',updateVideoControlSlider);dom.player.addEventListener('ended',playerEnded);dom.videoContainer.addEventListener('click',toggleVideoControls);dom.timeSlider.addEventListener('keypress',handleSliderKeypress);}
function initOptionsButtons(){dom.optionsCancelButton.addEventListener('click',hideOptionsView);addEventListeners('.single-delete-button','click',deleteCurrentVideo);addEventListeners('.single-share-button','click',shareCurrentVideo);addEventListeners('.single-info-button','click',showInfoView);}
function addEventListeners(selector,type,listener){var elements=document.body.querySelectorAll(selector);for(var i=0;i<elements.length;i++){elements[i].addEventListener(type,listener);}}
function toggleFullscreenPlayer(e){if(currentLayoutMode===LAYOUT_MODE.list){switchLayout(LAYOUT_MODE.fullscreenPlayer);scheduleVideoControlsAutoHiding();}else{switchLayout(LAYOUT_MODE.list);}
VideoUtils.fitContainer(dom.videoContainer,dom.player,currentVideo.metadata.rotation||0);}
function toggleVideoControls(e){if(controlFadeTimeout){clearTimeout(controlFadeTimeout);controlFadeTimeout=null;}
if(!pendingPick){e.cancelBubble=!controlShowing;setControlsVisibility(!controlShowing);}}
function handleScreenLayoutChange(){isPortrait=ScreenLayout.getCurrentLayout('portrait');if(!isPhone){if(!isPortrait&&(!firstScanEnded||processingQueue)){setDisabled(dom.playerView,true);}else{setDisabled(dom.playerView,false);}
if(currentLayoutMode===LAYOUT_MODE.list){if(isPortrait){hidePlayer(true);}else{showPlayer(currentVideo,false,false,true);}}
ThumbnailItem.titleMaxLines=isPortrait?4:2;}
if(currentLayoutMode!==LAYOUT_MODE.fullscreenPlayer){if(!thumbnailList){return;}
thumbnailList.updateAllThumbnailTitles();}else{pendingUpdateTitleText=true;}
if(dom.player.readyState!==HAVE_NOTHING){VideoUtils.fitContainer(dom.videoContainer,dom.player,currentVideo.metadata.rotation||0);}}
function switchLayout(mode,ignoreUpdate){var oldMode=currentLayoutMode;if(oldMode){document.body.classList.remove(currentLayoutMode);}
currentLayoutMode=mode;document.body.classList.add(currentLayoutMode);if(oldMode===LAYOUT_MODE.fullscreenPlayer&&pendingUpdateTitleText){pendingUpdateTitleText=false;if(!ignoreUpdate){thumbnailList.updateAllThumbnailTitles();}}
if(window.NavigationMap!=undefined){NavigationMap.onPanelChanged(oldMode,currentLayoutMode);}}
function handleActivityEvents(a){var fromActivity=a.source.name;VideoUtils.fromActivity=true;if(fromActivity==='pick'){pendingPick=a;showPickView();}}
function showInfoView(){hideOptionsView();var length=isFinite(currentVideo.metadata.duration)?MediaUtils.formatDuration(currentVideo.metadata.duration):'';var size=isFinite(currentVideo.size)?MediaUtils.formatSize(currentVideo.size):'';var type=currentVideo.type;if(type){var index=currentVideo.type.indexOf('/');type=index>-1?currentVideo.type.slice(index+1):currentVideo.type;}
var resolution=(currentVideo.metadata.width&&currentVideo.metadata.height)?currentVideo.metadata.width+'x'+
currentVideo.metadata.height:'';var data={'info-name':currentVideo.metadata.title,'info-length':length,'info-size':size,'info-type':type,'info-date':MediaUtils.formatDate(currentVideo.date),'info-resolution':resolution};MediaUtils.populateMediaInfo(data);dom.infoView.classList.remove('hidden');dom.fileInfo.classList.remove('hidden');document.getElementById('infosection').scrollTop=0;document.body.classList.add(SUB_MODE.infoView);dom.fileInfo.classList.add('focus');dom.fileInfo.focus();NavigationMap.onPanelChanged(currentLayoutMode,SUB_MODE.infoView);}
function hideInfoView(notChangePannel){dom.infoView.classList.add('hidden');dom.fileInfo.classList.add('hidden');document.body.classList.remove(SUB_MODE.infoView);NavigationMap.onPanelChanged(SUB_MODE.infoView,currentLayoutMode);}
function showRenameView(){dom.thumbnailViews.classList.add('hidden');dom.fileRename.classList.remove('hidden');document.body.classList.add(SUB_MODE.renameView);dom.renameId.classList.add('focus');dom.renameId.focus();dom.renameId.value=currentVideo.name.substring(currentVideo.name.lastIndexOf("/")+1).split(".")[0];NavigationMap.onPanelChanged(currentLayoutMode,SUB_MODE.renameView);dom.renameId.oninput=function(){NavigationMap.setSoftKeyBar(SUB_MODE.renameView);};}
function hideRenameView(){dom.renameId.value=null;dom.fileRename.classList.add('hidden');dom.thumbnailViews.classList.remove('hidden');document.body.classList.remove(SUB_MODE.renameView);NavigationMap.onPanelChanged(SUB_MODE.renameView,currentLayoutMode);dom.renameId.oninput=null;}
function showSelectView(){hidePlayer(true,function(){switchLayout(LAYOUT_MODE.selection);thumbnailList.setSelectMode(true);clearSelection();});}
function hideSelectView(){clearSelection();thumbnailList.setSelectMode(false);switchLayout(LAYOUT_MODE.list);if(!isPhone&&!isPortrait&&currentVideo){showPlayer(currentVideo,false,false,true);}}
function showOptionsView(){if(playing){pause();}
dom.optionsView.classList.remove('hidden');document.body.classList.add('options-view');}
function hideOptionsView(){dom.optionsView.classList.add('hidden');document.body.classList.remove('options-view');}
function setDisabled(element,disabled){element.classList.toggle('disabled',disabled);element.setAttribute('aria-disabled',disabled);}
function setSelected(element,selected){element.classList.toggle('selected',selected);element.setAttribute('aria-selected',selected);}
function clearSelection(){Array.forEach(selectedFileNames,function(name){setSelected(thumbnailList.thumbnailMap[name].htmlNode,false);});selectedFileNames=[];selectedFileNamesToBlobs={};dom.thumbnailsNumberSelected.textContent=navigator.mozL10n.get('number-selected2',{n:0});}
function updateSelectedNumber(number){dom.thumbnailsNumberSelected.textContent=navigator.mozL10n.get('number-selected2',{n:number});}
function updateSelection(videodata){var thumbnail=thumbnailList.thumbnailMap[videodata.name];var selected=!thumbnail.htmlNode.classList.contains('selected');setSelected(thumbnail.htmlNode,selected);var filename=videodata.name;if(selected){selectedFileNames.push(filename);videodb.getFile(filename,function(blob){selectedFileNamesToBlobs[filename]=blob;});}
else{delete selectedFileNamesToBlobs[filename];var i=selectedFileNames.indexOf(filename);if(i!==-1)
selectedFileNames.splice(i,1);}
updateSelectedNumber(selectedFileNames.length);}
function launchCameraApp(){var a=new MozActivity({name:'record',data:{type:'videos'}});}
function resetCurrentVideo(){if(!currentVideo){return;}
var currentThumbnail=thumbnailList.thumbnailMap[currentVideo.name];currentThumbnail.htmlNode.classList.remove('focused');var nextThumbnail=thumbnailList.findNextThumbnail(currentVideo.name);if(nextThumbnail){currentVideo=nextThumbnail.data;nextThumbnail.htmlNode.classList.add('focused');}else{currentVideo=null;}}
function deleteSelectedItems(){if(selectedFileNames.length===0)
return;totalDeletedCount=selectedFileNames.length;window.option.hide();var dialogConfig={title:{id:'confirmation-title',args:{}},body:{id:'delete-n-items?',args:{n:selectedFileNames.length}},desc:{id:'',args:{}},cancel:{l10nId:'cancel',priority:1,callback:function(){window.option.show();}},confirm:{l10nId:'delete',priority:3,callback:function(){deletedItems=[];for(var i=0;i<selectedFileNames.length;i++){deletedItems.push(selectedFileNames[i]);deleteFile(selectedFileNames[i]);}
clearSelection();hideSelectView();}}};confirmDialog(dialogConfig);}
function deleteFile(filename){if(FROMCAMERA.test(filename)){var postername=filename.replace('.3gp','.jpg');navigator.getDeviceStorage('pictures').delete(postername);}
videodb.deleteFile(filename);}
function shareSelectedItems(){var blobs=selectedFileNames.map(function(name){return selectedFileNamesToBlobs[name];});share(blobs);}
function share(blobs){if(blobs.length===0)
return;var names=[],fullpaths=[];blobs.forEach(function(blob){var name=blob.name;fullpaths.push(name);name=name.substring(name.lastIndexOf('/')+1);names.push(name);});if(playerShowing){releaseVideo();}
var a=new MozActivity({name:'share',data:{type:'video/*',number:blobs.length,blobs:blobs,filenames:names,filepaths:fullpaths}});function reinitFocus(){setTimeout(function(){window.focus();},500)}
a.onsuccess=function(e){NavigationMap.disableNav=false;reinitFocus();restoreVideo();};a.onerror=function(e){if(a.error.name==='NO_PROVIDER'){var msg=navigator.mozL10n.get('share-noprovider');alert(msg);}else{console.warn('share activity error:',a.error.name);}
NavigationMap.disableNav=false;reinitFocus();restoreVideo();};}
function updateDialog(){if(thumbnailList.count!==0&&(!storageState||playerShowing)){showOverlay(null);}else if(storageState===MediaDB.UPGRADING){showOverlay('upgrade');}else if(storageState===MediaDB.NOCARD){showOverlay('nocard');}else if(storageState===MediaDB.UNMOUNTED){showOverlay('pluggedin');}else if(firstScanEnded&&thumbnailList.count===0&&metadataQueue.length===0){NavigationMap.setSoftKeyBar(LAYOUT_MODE.list);showOverlay('empty');}}
function thumbnailClickHandler(videodata){if(!isPhone&&!isPortrait){if(!firstScanEnded||processingQueue){return;}}
if(currentLayoutMode===LAYOUT_MODE.list){hidePlayer(true,function(){stopParsingMetadata(function(){var fullscreen=pendingPick||isPhone||isPortrait;showPlayer(videodata,true,fullscreen,pendingPick);});});}
else if(currentLayoutMode===LAYOUT_MODE.selection){updateSelection(videodata);NavigationMap.setSoftKeyBar(currentLayoutMode);}}
function setPosterImage(dom,poster){if(dom.dataset.uri){URL.revokeObjectURL(dom.dataset.uri);}
dom.dataset.uri=URL.createObjectURL(poster);dom.style.backgroundImage='url('+dom.dataset.uri+')';dom.style.backgroundSize='100% 100%';}
function showOverlay(id){LazyLoader.load('shared/style/confirm.css',function(){currentOverlay=id;if(id===null){document.body.classList.remove(SUB_MODE.overlay);dom.overlay.classList.add('hidden');dom.noVideo.classList.add('hidden');switchLayout(LAYOUT_MODE.list);return;}
var _=navigator.mozL10n.get;var text,title;if(pendingPick||id==='empty'){window.focus();}else{}
if(id==='nocard'){title='nocard2-title';text='nocard3-text';}else{title=id+'-title';text=id+'-text';}
dom.overlayTitle.setAttribute('data-l10n-id',title);dom.overlayText.setAttribute('data-l10n-id',text);dom.overlay.classList.remove('hidden');document.body.classList.add(SUB_MODE.overlay);dom.noVideo.classList.remove('hidden');});}
function setControlsVisibility(visible){if(isPhone||isPortrait||currentLayoutMode!==LAYOUT_MODE.list){dom.playerView.classList[visible?'remove':'add']('video-controls-hidden');controlShowing=visible;}else{controlShowing=true;}
if(visible){document.body.classList.remove('video-controls-hidden');document.getElementsByTagName('meta')['theme-color'].setAttribute('content','#48132c');}else{document.body.classList.add('video-controls-hidden');document.getElementsByTagName('meta')['theme-color'].setAttribute('content','');}
dom.videoContainer.setAttribute('data-l10n-id',controlShowing?'hide-controls-button':'show-controls-button');if(controlShowing){updateVideoControlSlider();}}
function movePlayHead(percent){dom.playHead.style.left=percent;}
function updateVideoControlSlider(){if(dom.player.seeking){return;}
var percent=(dom.player.currentTime/dom.player.duration)*100;if(isNaN(percent)){return;}
percent+='%';dom.elapsedText.textContent=MediaUtils.formatDuration(dom.player.currentTime);dom.elapsedTime.style.width=percent;var remainingTime=dom.player.duration-dom.player.currentTime;dom.durationText.textContent=(remainingTime>0)?'-'+MediaUtils.formatDuration(remainingTime):'---:--';if(!dragging){movePlayHead(percent);}}
function setVideoPlaying(playing){if(playing){play();}else{pause();}}
function deleteCurrentVideo(){hideOptionsView();window.option.hide();totalDeletedCount=1;var dialogConfig={title:{id:'confirmation-title',args:{}},body:{id:'delete-video?',args:{}},desc:{id:'',args:{}},cancel:{l10nId:'cancel',priority:1,callback:function(){if(currentLayoutMode===LAYOUT_MODE.fullscreenPlayer){setButtonPaused(true);}
window.option.show();}},confirm:{l10nId:'delete',priority:3,callback:function(){deletedItems=[currentVideo.name];deleteFile(currentVideo.name);if(!isPhone&&!isPortrait){if(currentVideo){showPlayer(currentVideo,false,true,true);}}else{if(currentLayoutMode===LAYOUT_MODE.list){hidePlayer(false);}else if(currentLayoutMode===LAYOUT_MODE.fullscreenPlayer){next();}}
NavigationMap.setSoftKeyBar(currentLayoutMode);}}};confirmDialog(dialogConfig);}
function handlePlayButtonClick(){setVideoPlaying(dom.player.paused);}
function handleCloseButtonClick(){if(isPhone||isPortrait){hidePlayer(true);}else{toggleFullscreenPlayer();}}
function postPickResult(){pendingPick.postResult({type:currentVideoBlob.type,blob:currentVideoBlob});}
function shareCurrentVideo(){hideOptionsView();videodb.getFile(currentVideo.name,function(blob){share([blob]);});}
function handleSliderTouchStart(event){if(null!=touchStartID){return;}
touchStartID=event.changedTouches[0].identifier;isPausedWhileDragging=dom.player.paused;dragging=true;sliderRect=dom.sliderWrapper.getBoundingClientRect();if(dom.player.duration===Infinity)
return;if(!isPausedWhileDragging){dom.player.pause();}
handleSliderTouchMove(event);}
function setVideoUrl(player,video,callback){function handleLoadedMetadata(){dom.player.onloadedmetadata=null;callback();}
function loadVideo(url){loadingChecker.ensureVideoLoads(handleLoadedMetadata);player.src=url;}
if('name'in video){videodb.getFile(video.name,function(file){var url=URL.createObjectURL(file);loadVideo(url);if(pendingPick)
currentVideoBlob=file;});}else if('url'in video){loadVideo(video.url);}}
function scheduleVideoControlsAutoHiding(){controlFadeTimeout=setTimeout(function(){setControlsVisibility(false);},250);}
function showPlayer(video,autoPlay,enterFullscreen,keepControls){if(currentVideo){var old=thumbnailList.thumbnailMap[currentVideo.name];old.htmlNode.classList.remove('focused');}
currentVideo=video;var thumbnail=thumbnailList.thumbnailMap[currentVideo.name];thumbnail.htmlNode.classList.add('focused');if(currentLayoutMode!=="layout-fullscreen-player"){updateDialog();}
dom.player.preload='metadata';function doneSeeking(){dom.player.onseeked=null;setControlsVisibility(true);if(autoPlay){play();}else{pause();setPosterImage(dom.player,currentVideo.metadata.bookmark||currentVideo.metadata.poster);}
dom.player.hidden=false;}
dom.player.hidden=true;setVideoUrl(dom.player,currentVideo,function(){if(enterFullscreen){switchLayout(LAYOUT_MODE.fullscreenPlayer);}
var formattedDuration=MediaUtils.formatDuration(dom.player.duration);dom.durationText.textContent=formattedDuration;timeUpdated();setButtonPaused(false);playerShowing=true;var rotation;if('metadata'in currentVideo){if(currentVideo.metadata.currentTime===dom.player.duration){currentVideo.metadata.currentTime=0;}
dom.player.currentTime=0;rotation=currentVideo.metadata.rotation;}else{dom.player.currentTime=0;rotation=0;}
navigator.mozL10n.setAttributes(dom.timeSlider,'seek-bar',{duration:formattedDuration});dom.timeSlider.setAttribute('aria-valuemin',0);dom.timeSlider.setAttribute('aria-valuemax',dom.player.duration);dom.timeSlider.setAttribute('aria-valuenow',dom.player.currentTime);dom.timeSlider.setAttribute('aria-valuetext',MediaUtils.formatDuration(dom.player.currentTime));VideoUtils.fitContainer(dom.videoContainer,dom.player,rotation||0);if(dom.player.seeking){dom.player.onseeked=doneSeeking;}else{doneSeeking();}});}
function hidePlayer(updateVideoMetadata,callback){if(!playerShowing){if(callback){callback();}
return;}
dom.player.style.backgroundImage='url()';dom.player.pause();playing=false;function completeHidingPlayer(){switchLayout(LAYOUT_MODE.list,true);setButtonPaused(false);playerShowing=false;updateDialog();dom.player.removeAttribute('src');dom.player.load();startParsingMetadata();if(callback){callback();}}
if(!('metadata'in currentVideo)||!updateVideoMetadata||pendingPick){completeHidingPlayer();return;}
var video=currentVideo;var thumbnail=thumbnailList.thumbnailMap[video.name];if(dom.player.currentTime===0){video.metadata.bookmark=null;}
function updateMetadata(){if(!video.metadata.watched){video.metadata.watched=true;thumbnail.setWatched(true);}
if(!isPhone){videodb.updateMetadata(video.name,video.metadata);}
completeHidingPlayer();}
updateMetadata();}
function playerEnded(){if(dragging){return;}
if(endedTimer){clearTimeout(endedTimer);endedTimer=null;}
if(playing){dom.player.currentTime=0;pause();}
if(isFullScreen){NavigationMap.updateFullScreenSet();}}
function setButtonPaused(paused){NavigationMap.updateSoftKey(paused);}
function play(){loadingChecker.ensureVideoPlays();setButtonPaused(false);VideoStats.start(dom.player);dom.player.play();playing=true;}
function pause(noUpdateSK){loadingChecker.cancelEnsureVideoPlays();if(noUpdateSK===undefined){setButtonPaused(true);}
if(dragging){dragging=false;dom.playHead.classList.remove('active');}
dom.player.pause();playing=false;VideoStats.stop();VideoStats.dump();}
function getcurrentIndex(){var keys=Object.keys(thumbnailList.thumbnailMap);var i;for(i=0;i<keys.length;i++){if(keys[i]===currentVideo.name){return i;}}
return 0;}
function next(){var videodata=thumbnailList.nextThumbnail(currentVideo.name).data;var fullscreen=pendingPick||isPhone||isPortrait;dom.player.style.backgroundImage='url()';showPlayer(videodata,playing,fullscreen,pendingPick);}
function previous(){var videodata=thumbnailList.prevThumbnail(currentVideo.name).data;var fullscreen=pendingPick||isPhone||isPortrait;dom.player.style.backgroundImage='url()';showPlayer(videodata,playing,fullscreen,pendingPick);}
function timeUpdated(){if(controlShowing){if(dom.player.duration===Infinity||dom.player.duration===0){return;}
updateVideoControlSlider();}
dom.timeSlider.setAttribute('aria-valuenow',dom.player.currentTime);dom.timeSlider.setAttribute('aria-valuetext',MediaUtils.formatDuration(dom.player.currentTime));if(!endedTimer){if(!dragging&&dom.player.currentTime>=dom.player.duration-1){var timeUntilEnd=(dom.player.duration-dom.player.currentTime+.5);endedTimer=setTimeout(playerEnded,timeUntilEnd*1000);}}else if(dragging&&dom.player.currentTime<dom.player.duration-1){clearTimeout(endedTimer);endedTimer=null;}}
function handleSliderTouchEnd(event){if(!event.changedTouches.identifiedTouch(touchStartID)){return;}
touchStartID=null;if(!dragging){return;}
dragging=false;dom.playHead.classList.remove('active');if(dom.player.currentTime===dom.player.duration){pause();}else if(!isPausedWhileDragging){dom.player.play();}}
function handleSliderTouchMove(event){if(!dragging){return;}
var touch=event.changedTouches.identifiedTouch(touchStartID);if(!touch){return;}
function getTouchPos(){return(navigator.mozL10n.language.direction==='ltr')?(touch.clientX-sliderRect.left):(sliderRect.right-touch.clientX);}
var touchPos=getTouchPos();var pos=touchPos/sliderRect.width;pos=Math.max(pos,0);pos=Math.min(pos,1);var percent=pos*100+'%';dom.playHead.classList.add('active');movePlayHead(percent);dom.elapsedTime.style.width=percent;dom.player.fastSeek(dom.player.duration*pos);}
function handleSliderKeypress(event){var step=Math.max(dom.player.duration/20,2);if(event.keyCode===event.DOM_VK_DOWN){dom.player.fastSeek(dom.player.currentTime-step);}else if(event.keyCode===event.DOM_VK_UP){dom.player.fastSeek(dom.player.currentTime+step);}}
function toCamelCase(str){return str.replace(/\-(.)/g,function replacer(str,p1){return p1.toUpperCase();});}
function showPickView(){thumbnailList.setPickMode(true);document.body.classList.add(SUB_MODE.pickActivity);if(!isPhone&&!isPortrait){thumbnailList.updateAllThumbnailTitles();}}
function cancelPick(){pendingPick.postError('pick cancelled');cleanupPick();VideoUtils.fromActivity=false;}
function cleanupPick(){pendingPick=null;currentVideoBlob=null;hidePlayer(false);}
function releaseVideo(){if(videoHardwareReleased){return;}
videoHardwareReleased=true;if(dom.player.readyState>0){restoreTime=dom.player.currentTime;}
else{restoreTime=0;}
captureFrame(dom.player,currentVideo.metadata,function(sharemark){setPosterImage(dom.player,sharemark)});var videoWidth=dom.player.videoWidth;var videoHeight=dom.player.videoHeight;dom.player.removeAttribute('src');dom.player.load();dom.player.style.width=videoWidth+'px';dom.player.style.height=videoHeight+'px';}
function restoreVideo(){if(!videoHardwareReleased){return;}
videoHardwareReleased=false;dom.player.style.backgroundImage='url()';setVideoUrl(dom.player,currentVideo,function(){VideoUtils.fitContainer(dom.videoContainer,dom.player,currentVideo.metadata.rotation||0);dom.player.currentTime=restoreTime;});if(currentLayoutMode===LAYOUT_MODE.fullscreenPlayer){NavigationMap.updateSoftKey(true);}}
window.addEventListener('storage',function(e){if(e.key==='view-activity-wants-to-use-hardware'&&e.newValue&&!document.hidden&&playerShowing&&!videoHardwareReleased){console.log('The video app view activity needs to play a video.');console.log('Pausing the video and returning to the thumbnails.');console.log('See bug 1088456.');handleCloseButtonClick();}});window.addEventListener('index-changed',(e)=>{var item=e.detail.focusedItem;for(var filename in thumbnailList.thumbnailMap){if(thumbnailList.thumbnailMap[filename].htmlNode==item){currentVideo=thumbnailList.thumbnailMap[filename].data;break;}}});function seekVideo(seekTime){if(seekTime>=dom.player.duration){seekTime=0;pause();}
else if(seekTime<0){seekTime=0;}
dom.player.fastSeek(seekTime);};function renameVideo(oldName,newName){videodb.getFile(oldName,function(blob){videodb.addFile(newName,blob,function(){var data=thumbnailList.thumbnailMap[oldName].data;data.name=newName;thumbnailList.addItem(data);videodb.deleteFile(oldName,function(){thumbnailList.removeItem(oldName);hideRenameView();Toaster.showToast({messageL10nId:'rename-saved',latency:2000,});});});});};function confirmDialog(dialogConfig){if(typeof ConfirmDialogHelper==='undefined'){LazyLoader.load('js/shared/confirm_dialog_helper.js',()=>{var dialog=new ConfirmDialogHelper(dialogConfig);dialog.show(document.getElementById('confirm-dialog-container'));});}else{var dialog=new ConfirmDialogHelper(dialogConfig);dialog.show(document.getElementById('confirm-dialog-container'));}}